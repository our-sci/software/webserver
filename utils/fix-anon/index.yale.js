/**
 * How to run
 * change into this dir
 * adjust toAnon
 * then copy ALL files from certain survey to xml/
 * then run
 * node index.js
 * only qualifiying files will then land in out/
 *
 * next cd OursciServer/utils/buckets
 * aws s3 sync --profile oursci-s3 net-oursci s3://net-oursci --dryrun
 *
 * to dry run
 *
 *
 * then
 * aws s3 sync --profile oursci-s3 net-oursci s3://net-oursci
 *
 */
// put uuid:fasfasfasf files into xml folder

const path = require("path");
var fs = require("fs"),
  xml2js = require("xml2js");

const toAnon = [
  '/data/Point_data/Location:label',
  '/data/Point_data/Cluster:label',
  '/data/Point_data/Bearing:label',
  '/data/Point_data/Point:label',
  '/data/Point_data/Inaccessible:label',
  '/data/Point_data/GPS_location:label',
  '/data/Point_data/Bare_ground:label',
  '/data/Point_data/Rock:label',
  '/data/Point_data/Litter:label',
  '/data/Point_data/Forbs:label',
  '/data/Point_data/Grass:label',
  '/data/Point_data/Cheatgrass:label',
  '/data/Point_data/Woody:label',
  '/data/Point_data/Actual_depth_15:label',
  '/data/Point_data/Wet_weight_15-full:label',
  '/data/Point_data/QR_code_15:label',
  '/data/Point_data/Issues_15:label',
  '/data/Point_data/Actual_depth_30:label',
  '/data/Point_data/Wet_weight_30-full:label',
  '/data/Point_data/QR_code_30:label',
  '/data/Point_data/Issues_30:label',
];

var parser = new xml2js.Parser();
var builder = new xml2js.Builder();

const inputFiles = [];
const toWrite = [];

const p = __dirname + "/xml/";

fs.readdirSync(p)
  .filter(f => fs.statSync(path.join(p, f)).isDirectory())
  .forEach(dir => {
    inputFiles.push(dir);
  });

console.log(inputFiles);

function hasAll(node, anon) {
  const v = node.map(e => e.id)[0];

  let r = true;
  anon.forEach(e => {
    if (!v.includes(e)) {
      r = false;
    }
  });

  return r;
}

inputFiles.forEach(f => {
  const data = fs.readFileSync(path.join(p, f, "survey.xml"));
  parser.parseString(data, function(err, result) {
    if (
      result.data.meta[0].anonymize === undefined ||
      !hasAll(result.data.meta[0].anonymize, toAnon)
    ) {
      result.data.meta[0].anonymize = [
        {
          id: toAnon
        }
      ];
      const obj = {};
      obj[f] = builder.buildObject(result);
      toWrite.push(obj);
    }
  });
});

toWrite.forEach(item => {
  const uuid = Object.keys(item)[0];
  console.log("writing file");
  console.log(uuid);
  const dir = path.join(__dirname, "out", uuid);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  fs.writeFileSync(path.join(dir, "survey.xml"), item[uuid], "utf-8");
});
