// put uuid:fasfasfasf files into xml folder


const path = require("path");
var fs = require("fs"),
  xml2js = require("xml2js");

var parser = new xml2js.Parser();
var builder = new xml2js.Builder();

const inputFiles = [];
const toWrite = [];

const p = __dirname + "/xml/";

fs.readdirSync(p)
  .filter(f => fs.statSync(path.join(p, f)).isDirectory())
  .forEach(dir => {
    inputFiles.push(dir);
  });

console.log(inputFiles);

inputFiles.forEach(f => {
  const data = fs.readFileSync(path.join(p, f, "survey.xml"));
  parser.parseString(data, function(err, result) {
    console.log(result.data.meta[0].anonymize);
    if (result.data.meta[0].anonymize === undefined) {
      result.data.meta[0].anonymize = [
        {
          id: [
            "/data/group_farm/farm_name:label",
            "/data/group_farm/followup:label",
            "/data/group_farm/farm_email:label"
          ]
        }
      ];

      const obj = {};
      obj[f] = builder.buildObject(result);
      toWrite.push(obj);
    }
  });
});

console.log(toWrite);

toWrite.forEach(item => {
  const uuid = Object.keys(item)[0];
  console.log("writing file");
  console.log(uuid);
  const dir = path.join(__dirname, "out", uuid);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  fs.writeFileSync(path.join(dir, "survey.xml"), item[uuid], "utf-8");
});

console.log(toWrite);
