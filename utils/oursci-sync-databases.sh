#!/bin/bash

ABSDIR=$(cd `dirname $0` && pwd)
TARGETDIR=$ABSDIR/databases
TIMESTAMP=$(date +%Y-%m-%dT%H:%M:%S%z)

LOCAL_DB_NAME=db_oursci_local
LOCAL_DB_USER=db_oursci_user
LOCAL_DB_PASSWORD=db_oursci_password

if [ ! -d "$TARGETDIR" ]; then
  mkdir $TARGETDIR
fi

# download dump
echo "Creating backup of production MYSQL database..."
echo "You may need to provide the master password for the production database"
mysqldump -u oursci -p  -h db-oursci-production.ckg5tg2pojth.us-east-1.rds.amazonaws.com db_oursci > $TARGETDIR/db_oursci.sql
cp $TARGETDIR/db_oursci.sql $TARGETDIR/db_oursci.sql.$TIMESTAMP

# dump to local database
echo "Copying backup into local database..."
mysql -u $LOCAL_DB_USER -p$LOCAL_DB_PASSWORD  $LOCAL_DB_NAME < $TARGETDIR/db_oursci.sql

