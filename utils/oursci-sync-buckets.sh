#!/bin/bash

ABSDIR=$(cd `dirname $0` && pwd)
TARGETDIR=$ABSDIR/buckets

if [ ! -d "$TARGETDIR" ]; then
  mkdir $TARGETDIR
fi

# Sync net-oursci to local folder
aws s3 sync --profile oursci-s3 s3://net-oursci $TARGETDIR/net-oursci

# hard copy (sync only updates unchanged files) 
# aws s3 cp --profile oursci-s3 s3://net-oursci s3://net-oursci-testing --recursive --acl public-read

# Sync net-oursci to net-oursci-testing bucket
aws s3 sync --profile oursci-s3 s3://net-oursci s3://net-oursci-testing

# And finally net-oursci-testing to local folder
aws s3 sync --profile oursci-s3 s3://net-oursci-testing $TARGETDIR/net-oursci-testing
