import React, { Component } from 'react';
import { connect } from 'react-redux';


import {
  logoutUser
} from '../actions/auth';

import Navbar, {
  NavbarNav,
  ItemLink,
  Dropdown,
  Separator
}
from './nav';

import C from '../constants';

class Header extends Component {

  constructor(props) {
    super(props);
    this.renderLoginLogoutLink = this.renderLoginLogoutLink.bind(this);
    this.renderProfileLink = this.renderProfileLink.bind(this);
  }

  renderProfileLink() {
    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return null;
    }

    return (
      <ItemLink to='/resources/client/profile'>Profile</ItemLink>
    );
  }

  renderLoginLogoutLink() {
    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return <ItemLink to='/login'><span className='glyphicon glyphicon-log-in' /> Sign in</ItemLink>;
    }

    return (
      <ItemLink to='/' onClick={this.props.logoutUser}><span className='glyphicon glyphicon-log-out' /> Sign out</ItemLink>
    );
  }

  render() {
    return (
      <Navbar title='Oursci Web'>
        <NavbarNav>
          <ItemLink to='/survey'>Surveys</ItemLink>
          <ItemLink to='/script'>Scripts</ItemLink>
        </NavbarNav>

        <NavbarNav alignment='right'>
          {this.renderProfileLink()}
          {this.renderLoginLogoutLink()}

          {/*
          <Dropdown title='More'>
            <ItemLink to='/resources/open/example'>Open</ItemLink>
            <ItemLink to='/resources/client/example'>Client</ItemLink>
            <ItemLink to='/resources/admin/example'>Admin</ItemLink>
            <Separator />
            {this.renderLoginLogout()}
            <Separator />
          </Dropdown>
          */}
        </NavbarNav>
      </Navbar>
    );
  }
}

const mapStateToProps = state => ({ auth: state.auth });

const mapDispatchToProps = {
  logoutUser
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
