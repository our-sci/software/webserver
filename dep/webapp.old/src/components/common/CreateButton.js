import React from 'react';
import { Link } from 'react-router-dom';

const CreateButton = ({ to, children }) => {
  return (
    <Link to={to} type="button" className="btn btn-success">
    <span
      className="glyphicon glyphicon-plus"
      aria-hidden="true"
    /> {children}
    </Link>
  );
};

export default CreateButton;
