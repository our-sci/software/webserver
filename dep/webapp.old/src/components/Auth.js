import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { Link, withRouter } from 'react-router-dom';

import {
  openAuthWithProvider,
  openAuthWithEmailAndPassword,
  registerWithEmailAndPassword,
  logoutUser,
  emailChanged,
  passwordChanged
} from '../actions/auth';
import C from '../constants';
import SocialButton from './SocialButton';
import Token from './common/Token';

class Auth extends Component {

  constructor(props) {
    super(props);

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAuthWithProvider = this.handleAuthWithProvider.bind(this);
    this.handleAuthWithEmailAndPassword = this.handleAuthWithEmailAndPassword.bind(this);
    this.handleRegisterWithEmailAndPassword = this.handleRegisterWithEmailAndPassword.bind(this);
    this.renderLoginLogout = this.renderLoginLogout.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.auth.status !== nextProps.auth.status) {
      if (nextProps.auth.status === C.AUTH_LOGGED_IN) {
        this.props.history.push('/survey');
      }
    }
  }

  handleEmailChange(event) {
    this.props.emailChanged(event.target.value);
  }

  handlePasswordChange(event) {
    this.props.passwordChanged(event.target.value);
  }

  handleSubmit(event) {
    console.log('submitting form, preventing default..');
    event.preventDefault();
    this.handleAuthWithEmailAndPassword();
  }

  handleAuthWithProvider(provider) {
    return () => { this.props.openAuthWithProvider(provider); };
  }

  handleAuthWithEmailAndPassword() {
    const { email, password } = this.props.auth;
    this.props.openAuthWithEmailAndPassword(email, password);
  }

  handleRegisterWithEmailAndPassword() {
    const { email, password } = this.props.auth;
    this.props.registerWithEmailAndPassword(email, password);
  }

  renderLoginLogout() {
    const { status, token } = this.props.auth;

    if (status === C.AUTH_LOGGED_IN) {
      return (
        <div>
        <span>Logged in as {this.props.auth.username}.</span>
        {' '}<button onClick={this.props.logoutUser}>Log out</button>
        <Token token={token} />
        </div>
      );
    }

    return (
      <div className='container'>
      {(() => {
        if (status === C.AUTH_AWAITING_RESPONSE) {
          return <h2>Signing in... <span className="fa fa-spinner fa-spin" /></h2>;
        }
        return <h2>Sign in</h2>;
      })()}
      <div className='row'>
      <div className='col-md-4'>
      <form onSubmit={this.handleSubmit}>
      <div className='form-group'>
      <label htmlFor='email'>E-Mail:</label>
      <input
      className='form-control'
      id='email'
      value={this.props.auth.email}
      onChange={this.handleEmailChange}
      />
      </div>
      <div className='form-group'>
      <label htmlFor='password'>Password:</label>
      <input
      className='form-control'
      id='password'
      type='password'
      value={this.props.auth.password}
      onChange={this.handlePasswordChange}
      />
      </div>

      <div className='btn-toolbar pull-right'>
      <button
      type='button'
      onClick={this.handleRegisterWithEmailAndPassword}
      className='btn btn-info'
      >Register</button>
      <button
      type='submit'
      className='btn btn-success'
      >Sign in</button>


      </div>
      </form>
      </div>
      </div>

      <div className='row'>
      <div className='col-md-4'>
      <hr />
      <SocialButton
      provider='facebook'
      onClick={this.handleAuthWithProvider(new firebase.auth.FacebookAuthProvider())}
      >Sign in with Facebook</SocialButton>
      <SocialButton
      provider='google'
      onClick={this.handleAuthWithProvider(new firebase.auth.GoogleAuthProvider())}
      >Sign in with Google</SocialButton>
      </div> {/* div row end */}
      </div> {/* div col end */}

      </div>
    );
  }


  render() {
    return (
      <div>
      {this.renderLoginLogout()}
      </div>
    );
  }
}

const mapStateToProps = state => ({ auth: state.auth });

const mapDispatchToProps = {
  openAuthWithProvider,
  openAuthWithEmailAndPassword,
  registerWithEmailAndPassword,
  logoutUser,
  emailChanged,
  passwordChanged
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Auth));
