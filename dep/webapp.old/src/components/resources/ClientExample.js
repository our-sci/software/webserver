import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getClientExample } from '../../actions/api';
import C from '../../constants';


class ClientExample extends Component {

  constructor(props) {
    super(props);

    this.renderExample = this.renderExample.bind(this);
  }

  componentDidMount() {
    const { token } = this.props.auth;
    if (token) {
      this.props.getClientExample(token);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.auth.token && nextProps.auth.token) {
      this.props.getClientExample(nextProps.auth.token);
    }
  }

  renderExample() {
    const { example } = this.props.api.client;

    if (example.status === C.STATUS_LOADING) {
      return <li>loading...</li>;
    }

    if (example.status === C.STATUS_ERROR) {
      return null;
    }

    return <li>Action: {example.data.action}, api: {example.data.api}</li>;
  }

  render() {
      return (
        <div>
        <h2>Client</h2>
        <ul>
        {this.renderExample()}
        </ul>
        </div>
      );
  }

}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

const mapDispatchToProps = {
  getClientExample
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientExample);
