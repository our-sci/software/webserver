import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Uploader from '../common/Uploader';

const ACCEPTED_FILES = [
  //{ type: 'application/zip', desc: 'ZIP archives (*.zip)' },
  //{ type: 'application/x-tar', desc: 'TAR archives (*.tar)' },
  { type: '.json', desc: 'JSON files' },
  { type: 'application/javascript', desc: 'Javascript files' },

];

const initialState = { files: [], error: null, override: false };

class SurveyCreate extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;

    this.onSuccess = this.onSuccess.bind(this);
  }

  onSuccess() {
    console.log('all good');
    this.props.history.push('/script');
  }

  render() {
    return (
      <Uploader
        name='Script'
        url='/api/script/create'
        acceptedFiles={ACCEPTED_FILES}
        onSuccess={this.onSuccess}
      />
    );
  }

}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

export default withRouter(connect(mapStateToProps, null)(SurveyCreate));
