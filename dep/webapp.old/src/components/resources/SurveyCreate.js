import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Uploader from '../common/Uploader';


const ACCEPTED_FILES = [
  //{ type: 'application/zip', desc: 'ZIP archives (*.zip)' },
  //{ type: 'application/x-tar', desc: 'TAR archives (*.tar)' },
  { type: 'text/xml', desc: 'XML files' },
  { type: 'image/*', desc: 'Any type of images' },
  { type: 'text/plain', desc: 'Plain text files' },
  { type: 'application/javascript', desc: 'Javascript files' }
];

const initialState = { files: [], error: null, override: false };

class SurveyCreate extends Component {

  constructor(props) {
    super(props);
    this.state = initialState;

    this.onSuccess = this.onSuccess.bind(this);
  }

  onSuccess() {
    this.props.history.push('/survey');
  }

  render() {
    return (
      <Uploader
        name='Survey'
        url='/api/survey/create'
        acceptedFiles={ACCEPTED_FILES}
        onSuccess={this.onSuccess}
      />
    );
  }

}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

export default withRouter(connect(mapStateToProps, null)(SurveyCreate));
