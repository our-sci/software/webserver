import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import _ from 'lodash';

import { getSurveys } from '../../actions/api';
import C from '../../constants';
import CreateButton from '../common/CreateButton';


class SurveyList extends Component {
  constructor(props) {
    super(props);

    this.renderSurveys = this.renderSurveys.bind(this);
    this.viewSurvey = this.viewSurvey.bind(this);
    this.renderArchiveToggle = this.renderArchiveToggle.bind(this);

    this.state = { showArchived: false };
  }

  componentDidMount() {
    this.props.getSurveys();
  }

  viewSurvey(formId) {
    return () => {
      this.props.history.push(`/survey/by-form-id/${formId}`);
    };
  }

  renderSurveys() {
    const { surveys } = this.props.api.open;

    if (surveys.status === C.STATUS_LOADING) {
      return <tr><td>loading...</td></tr>;
    }

    if (surveys.status === C.STATUS_ERROR) {
      return <tr><td>Error: {surveys.data}</td></tr>;
    }

    return _.map(surveys.data, (survey) => {
      if (survey.archived && !this.state.showArchived) {
        return null;
      }

      return (
        <tr
          style={survey.archived ? { opacity: '0.5' } : {}}
          role='button' key={survey.formId}
          onClick={this.viewSurvey(survey.formId)}
        >
          <td><strong>{survey.formTitle}</strong></td>
          <td><small>{survey.formId}</small></td>
        </tr>
      );
    });
  }

  renderArchiveToggle() {
    return (
      <button
        onClick={() => this.setState({ showArchived: !this.state.showArchived })}
        className='btn btn-info'
      >{this.state.showArchived ? 'Hide Archived' : 'Show Archived'}
      </button>
    );
  }

  render() {
      return (
        <div>
        <div className='btn-toolbar pull-right'>
        {this.renderArchiveToggle()}
        <CreateButton to='/survey/create'>Create Survey</CreateButton>
        </div>

        <h2>Surveys</h2>
        <table className='table table-hover'>
          <thead><tr><th>Title</th><th>formId</th></tr></thead>
          <tbody>
          {this.renderSurveys()}
          </tbody>
        </table>
        </div>
      );
  }
}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

const mapDispatchToProps = {
  getSurveys
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyList);
