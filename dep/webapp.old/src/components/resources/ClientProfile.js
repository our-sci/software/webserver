import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getClientProfile } from '../../actions/api';
import C from '../../constants';
import Token from '../common/Token';


class ClientExample extends Component {

  constructor(props) {
    super(props);

    this.renderProfile = this.renderProfile.bind(this);
    this.renderProfileList = this.renderProfileList.bind(this);
    this.renderProfilePicture = this.renderProfilePicture.bind(this);
  }

  renderProfileList(profile) {
    return _.map(profile.data, (v, k) => {
      return <li key={k}>{k}: {v}</li>;
    });
  }

  renderProfilePicture(profile) {
    return <img alt='profile' height='100' width='100' src={profile.data.picture} />;
  }

  renderTable(profile) {
    return (
      <table className='table'>
      <thead>
        <tr><th>Key</th><th>Value</th></tr>
      </thead>
      <tbody>
        {
          _.map(profile.data, (v, k) => {
            return <tr key={k}><td>{k}</td><td>{v}</td></tr>;
          })
        }
      </tbody>
      </table>
    );
  }

  renderProfile() {
    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return <span>You are not signed in.</span>;
    }

    const { profile } = this.props.api.client;

    if (profile.status === C.STATUS_LOADING) {
      return <span>loading...</span>;
    }

    if (profile.status === C.STATUS_ERROR) {
      return <span>Error: {profile.data}</span>;
    }

    return (
      <div>
      {this.renderProfilePicture(profile)}
      {this.renderTable(profile)}
      <Token token={this.props.auth.token} />
      </div>);
  }

  render() {
      return (
        <div>
        <h2>Profile</h2>

        {this.renderProfile()}

        </div>
      );
  }
}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

const mapDispatchToProps = {
  getClientProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientExample);
