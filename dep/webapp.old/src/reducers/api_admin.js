import C from '../constants';

const INITIAL_STATE = {
  example: {
    data: null,
    status: C.STATUS_LOADING
  }
};

export default (state, action) => {
  switch (action.type) {
    case C.API_ADMIN_EXAMPLE_LOADING:
      return { ...state, example: { data: null, status: C.STATUS_LOADING } };
    case C.API_ADMIN_EXAMPLE_SUCCESS:
      return { ...state, example: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_ADMIN_EXAMPLE_ERROR:
      return { ...state, example: { data: action.payload, status: C.STATUS_ERROR } };
    default:
      return state || INITIAL_STATE;
  }
};
