import firebase from './firebase';
import auth from './auth';
import feedback from './feedback';
import api from './api';
import status from './status';
import date from './date';


export default {
  ...firebase,
  ...auth,
  ...feedback,
  ...api,
  ...status,
  ...date
};
