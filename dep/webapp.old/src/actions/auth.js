import axios from 'axios';

import C from '../constants';
import { auth } from '../firebaseApp';
import { getClientProfile } from './api';


const registerUserOnServer = (token, dispatch) => {
    axios({
      url: '/api/signup',
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'firebaseToken': token
      }
      })
      .then((response) => {
        dispatch({
          type: C.API_REGISTER_SUCCESS,
          payload: response.data
        });
        dispatch(getClientProfile(token));
      })
      .catch((error) => {
        dispatch({
          type: C.API_REGISTER_ERROR,
          payload: error.message
        });
        dispatch(getClientProfile(token));
      });
};


export const listenToAuth = () => (dispatch, getState) => {
  auth.onIdTokenChanged(authData => {
    if (authData) {
      dispatch({
        type: C.AUTH_LOGIN,
        uid: authData.uid,
        username: authData.displayName || authData.email
      });
      authData.getIdToken().then(token => {
          registerUserOnServer(token, dispatch);
          dispatch({
            type: C.AUTH_TOKEN_CHANGED,
            payload: token
          });
        });
    } else if (getState().auth.status !== C.AUTH_ANONYMOUS) {
        dispatch({ type: C.AUTH_LOGOUT });
    }
  });
};

export const openAuthWithProvider = (provider) => dispatch => {
  dispatch({ type: C.AUTH_OPEN });

  auth.signInWithPopup(provider).catch(error => {
    dispatch({
      type: C.FEEDBACK_DISPLAY_ERROR,
      error: `Login failed! ${error}`
    });
    dispatch({ type: C.AUTH_LOGOUT });
  });
};

export const openAuthWithEmailAndPassword = (email, password) => dispatch => {
  dispatch({ type: C.AUTH_OPEN });

  auth.signInWithEmailAndPassword(email, password).catch(error => {
    dispatch({
      type: C.FEEDBACK_DISPLAY_ERROR,
      error: `Login failed! ${error}`
    });
    dispatch({ type: C.AUTH_LOGOUT });
  });
};

export const registerWithEmailAndPassword = (email, password) => dispatch => {
  auth.createUserWithEmailAndPassword(email, password).catch(error => {
    dispatch({
      type: C.FEEDBACK_DISPLAY_ERROR,
      error: `User registration failed! ${error}`
    });
    dispatch({ type: C.AUTH_LOGOUT });
  });
};

export const logoutUser = () => dispatch => {
  dispatch({ type: C.AUTH_LOGOUT });
  auth.signOut();
};

export const emailChanged = (text) => {
  return {
    type: C.AUTH_EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: C.AUTH_PASSWORD_CHANGED,
    payload: text
  };
};
