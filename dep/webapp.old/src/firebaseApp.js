import * as firebase from 'firebase';

import C from './constants';

firebase.initializeApp(C.firebaseConfig);
export const auth = firebase.auth();
export const database = firebase.database();
