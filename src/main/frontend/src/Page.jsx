import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Feedback from './components/Feedback';
import Header from './components/Header';
import Footer from './components/Footer';
import SideBar from './components/SideBar';

import Auth from './pages/Auth';

import Profile from './pages/Profile';
import SurveyList from './pages/SurveyList';
import SurveySingle from './pages/SurveySingle';
import SurveyCreate from './pages/SurveyCreate';

import LandingPage from './pages/LandingPage';
import MobileApp from './pages/MobileApp';

import ScriptList from './pages/ScriptList';
import ScriptSingle from './pages/ScriptSingle';
import ScriptCreate from './pages/ScriptCreate';

import DashboardList from './pages/DashboardList';
import DashboardSingle from './pages/DashboardSingle';
import DashboardCreate from './pages/DashboardCreate';
import DashboardFrame from './pages/DashboardFrame';

import OrganizationList from './pages/OrganizationList';
import OrganizationCreate from './pages/OrganizationCreate';
import OrganizationSingle from './pages/OrganizationSingle';
import OrganizationEdit from './pages/OrganizationEdit';
import OrganizationMembersEdit from './pages/OrganizationMembersEdit';
import OrganizationFavoritesEdit from './pages/OrganizationFavoritesEdit';

import TpCredentialEdit from './pages/TpCredentialEdit';
import TpCredentialAdd from './pages/TpCredentialAdd';

import OrganizationSelect from './pages/OrganizationSelect';

import Share from './pages/Share';
import ShareCode from './pages/ShareCode';

import Admin from './pages/Admin';

import Macro from './pages/Macro';

const Page = () => (
  <div id="page">
    <Header />

    <div className="container-fluid main-container">
      <div className="row flex-xl-nowrap">
        <SideBar />

        <main className="col-12 col-md-9 py-md-3 px-md-5 os-content">
          <Feedback />
          <Switch>
            <Route exact path="/" component={LandingPage} />
            <Route exact path="/login" component={Auth} />
            <Route path="/login/invitation/:code" component={Auth} />
            <Route exact path="/share" component={Share} />
            <Route path="/share/view/:code" component={ShareCode} />

            <Route exact path="/survey" component={SurveyList} />

            <Route exact path="/landing" component={LandingPage} />

            <Route exact path="/script" component={ScriptList} />
            <Route path="/script/by-script-id/:scriptId" component={ScriptSingle} />
            <Route path="/script/by-database-id/:databaseId" component={ScriptSingle} />
            <Route path="/script/create" component={ScriptCreate} />

            <Route exact path="/dashboard" component={DashboardList} />

            <Route path="/dashboard/create" component={DashboardCreate} />

            <Route exact path="/organization" component={OrganizationList} />
            <Route path="/organization/create" component={OrganizationCreate} />

            <Route path="/organization/:organizationId/edit" component={OrganizationEdit} />
            <Route path="/organization/:organizationId/edit-members" component={OrganizationEdit} />
            <Route
              path="/organization/:organizationId/edit-invitations"
              component={OrganizationEdit}
            />
            <Route
              path="/organization/:organizationId/edit-favorites"
              component={OrganizationEdit}
            />
            <Route path="/organization/:organizationId/edit-assets" component={OrganizationEdit} />
            <Route
              path="/organization/:organizationId/edit-preferences"
              component={OrganizationEdit}
            />

            <Route
              path="/organization/:organizationId/members/add"
              component={OrganizationMembersEdit}
            />
            <Route
              path="/organization/:organizationId/members/edit"
              component={OrganizationMembersEdit}
            />
            <Route
              path="/organization/:organizationId/favorites/add"
              component={OrganizationFavoritesEdit}
            />
            <Route
              path="/organization/:organizationId/favorites/edit"
              component={OrganizationFavoritesEdit}
            />
            <Route path="/organization/:organizationId" component={OrganizationSingle} />

            <Route path="/tpcredential/add/:uid" component={TpCredentialAdd} />
            <Route path="/tpcredential/edit/:id" component={TpCredentialEdit} />

            <Route exact path="/profile" component={Profile} />
            <Route path="/profile/:uid" component={Profile} />

            <Route path="/survey/create" component={SurveyCreate} />
            <Route path="/survey/by-form-id/:formId" component={SurveySingle} />
            <Route path="/survey/by-database-id/:databaseId" component={SurveySingle} />

            <Route path="/o/:organizationId" component={OrganizationSelect} />

            <Route path="/macro" component={Macro} />

            {['/admin/users', '/admin/tpcredentials', '/admin/resubmissions'].map((path) => (
              <Route key={path} path={path} component={Admin} />
            ))}

            <Route path="/mobile-app" component={MobileApp} />
          </Switch>
          <Switch>
            <Route path="/dashboard/by-dashboard-id/:dashboardId" component={DashboardSingle} />
            <Route path="/dashboard/by-database-id/:databaseId" component={DashboardSingle} />
          </Switch>
          <Switch>
            <Route path="/dashboard/by-dashboard-id/:dashboardId" component={DashboardFrame} />
            <Route path="/dashboard/by-database-id/:databaseId" component={DashboardFrame} />
          </Switch>
        </main>
      </div>
    </div>

    <Footer />
  </div>
);

export default Page;
