import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import LinkButton from '../LinkButton';

import { isAdmin } from '../../actions/api';

class Members extends Component {
  render = () => {
    const { organization } = this.props;
    const { organizationId } = organization;

    return (
      <div>
        <table className="table table-striped table-hover">
          <thead>
            <tr className="border-top-0">
              <th>Email</th>
              <th>Display Name</th>
              <th>Admin</th>
              <th>Role</th>
              <th>Description</th>
              <th style={{ width: '6em' }} className="text-right">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {_.map(organization.members, (member) => (
              <tr key={member.user.username}>
                <td>{member.user.email}</td>
                <td>{member.user.displayName}</td>
                <td>{member.admin ? 'Y' : 'N'}</td>
                <td>{member.role}</td>
                <td>{member.description}</td>
                <td className="text-right">
                  <Link
                    to={`/organization/${organizationId}/members/edit?id=${member.id}`}
                    title="Edit"
                  >
                    <i className="fas fa-edit fa-lg" />
                  </Link>
                  <Link className="ml-2" to={`/profile/${member.user.username}`} title="Profile">
                    <i className="fas fa-user fa-lg" />
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {this.props.isAdmin() && (
          <div className="btn-toolbar float-right">
            <LinkButton to={`/organization/${organizationId}/members/add`} icon="fas fa-user-plus">
              Add Existing User
            </LinkButton>
          </div>
        )}
      </div>
    );
  };
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = {
  isAdmin,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Members);
