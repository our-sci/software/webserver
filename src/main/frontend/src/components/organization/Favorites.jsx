import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import ModalOrLink from '../ModalOrLink';
import LinkButton from '../LinkButton';

class Favorites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favoriteConfirmInsertion: null,
    };
  }

  renderFavoriteInsertionLink = (favorite) => {
    const { favoriteConfirmInsertion } = this.state;
    let url = '';
    if (favorite.type === 'survey') {
      url = `/#/survey/by-form-id/${favorite.typeIdentifier}`;
    } else if (favorite.type === 'script') {
      url = `/#/script/by-script-id/${favorite.typeIdentifier}`;
    } else if (favorite.type === 'dashboard') {
      url = `/#/dashboard/by-dashboard-id/${favorite.typeIdentifier}`;
    }
    const standardMarkdown = `[${favorite.typeIdentifier}](${url})`;

    return (
      <ModalOrLink
        showModal={favoriteConfirmInsertion === favorite}
        onHide={() => this.setState({ favoriteConfirmInsertion: null })}
        title={
          <span>
            Copy the following content, and paste into <strong>Content</strong>
</span>
        }
        onLinkClicked={() => this.setState({ favoriteConfirmInsertion: favorite })}
        link={<span className="fas fa-paperclip fa-lg" />}
      >
        {standardMarkdown}
      </ModalOrLink>
    );
  };

  renderFavoriteItem = (option) => {
    const DELIMITER = ';';

    let title = '';
    let identifier = '';

    switch (option.type) {
      case 'survey':
        title = option.survey.formTitle;
        identifier = option.survey.formId;
        break;
      case 'dashboard':
        title = option.dashboard.name;
        identifier = option.dashboard.dashboardId;
        break;
      case 'script':
        title = option.script.name;
        identifier = option.script.scriptId;
        break;
      default:
        title = 'Subtitle';
        identifier = 'identifier';
    }

    if (title.includes(DELIMITER)) {
      const mainTitle = title.substring(0, title.indexOf(DELIMITER));
      const subTitle = title.substring(title.indexOf(DELIMITER) + 1);
      return (
        <div style={{ marginBottom: 5 }}>
          {mainTitle}
          <br />
          <span style={{ fontSize: '95%' }}>{subTitle}</span>
          <br />
          <small>{identifier}</small>
        </div>
      );
    }

    return (
      <div style={{ marginBottom: 5 }}>
        {title}
        <br />
        <small>{identifier}</small>
      </div>
    );
  };

  render = () => {
    const { organization } = this.props;
    const { organizationId } = organization;

    return (
      <div>
        <table className="table table-striped table-hover">
          <thead>
            <tr className="border-top-0">
              <th style={{ width: 50 }} />
              <th>Type</th>
              <th>Item</th>
              <th>Description</th>
              <th>Priority</th>
              <th style={{ width: '80px' }}>Action</th>
            </tr>
          </thead>
          <tbody>
            {_.map(organization.favorites, (type, key) =>
              _.map(type, (favorite) => (
                <tr key={`${key}-${favorite.id}`}>
                  <td className="text-center align-middle">
                    {this.renderFavoriteInsertionLink(favorite)}
                  </td>
                  <td className="align-middle">{key}</td>
                  <td>{this.renderFavoriteItem(favorite)}</td>
                  <td>{favorite.description}</td>
                  <td>{favorite.priority}</td>
                  <td>
                    <Link to={`/organization/${organizationId}/favorites/edit?id=${favorite.id}`}>
                      EDIT
                    </Link>
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
        <div className="btn-toolbar float-right">
          <LinkButton to={`/organization/${organizationId}/favorites/add`} icon="fas fa-bookmark">
            Add Favorite
          </LinkButton>
        </div>
      </div>
    );
  };
}

export default Favorites;
