import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';

import ModalOrLink from '../ModalOrLink';

import { postOrganizationPreferences, getOrganizationById } from '../../actions/api';
import { cookieChooseOrganization } from '../../actions/cookie';

const INITIAL_STATE = {
  colorPrimary: '#000',
  colorSecondary: '#fff',
  colorTertiary: '#666',
  colorTextPrimary: '#888',
  colorTextPrimaryHover: '#fff',
  colorTextSecondary: '#000',
  colorTextSecondaryHover: '#333',
  borderWidth: '1',
  title: '',
  shortTitle: '',
  app: '',
  showIcon: false,
};

class OrganizationPreferences extends Component {
  constructor(props) {
    super(props);

    const { organization } = this.props;
    const { preferences } = organization;

    this.state = preferences || INITIAL_STATE;
  }

  uploadPreferences = ({ clear }) => {
    const { organizationId } = this.props.organization;

    this.props
      .postOrganizationPreferences({ organizationId, preferences: this.state, clear })
      .then(() => this.props.getOrganizationById(organizationId))
      .then((updatedOrganization) => {
        // if this is our currently selected organization, save it to cookies as well
        const { selected } = this.props.cookie.organization;
        if (selected && selected.organizationId === updatedOrganization.organizationId) {
          console.log('re-choosing cookie for organization');
          this.props.cookieChooseOrganization(updatedOrganization);
        }
      })
      .catch((error) => console.log(error.response));
  };

  clearPreferences = () => {
    this.uploadPreferences({ clear: true });
  };

  resetPreferences = () => {
    const { organization } = this.props;
    const { preferences } = organization;

    this.setState(preferences);
  };

  renderPref = ({ title, key, type }) => {
    if (type === 'checkbox') {
      return this.renderCheck({ title, key, type });
    }
    return this.renderInput({ title, key, type });
  };

  renderCheck = ({ title, key, type }) => (
    <div className="form-group col-md-6 col-lg-4">
      <label htmlFor={key}>{title}</label>
      <div className="input-group">
        <span className="input-group-addon">
          <input
            type="checkbox"
            checked={this.state[key] || false}
            onChange={(ev) => this.setState({ [key]: ev.target.checked })}
          />
        </span>
        <input
          className="form-control"
          id={key}
          value={this.state[key] ? 'yes' : 'no'}
          onChange={() => {}}
        />
      </div>
    </div>
  );

  renderInput = ({ title, key, type }) => (
    <div className="form-group col-md-6 col-lg-4">
      <label htmlFor={key}>
        {title}
        {type === 'color' && (
          <div
            style={{
              display: 'inline-block',
              border: '1px solid #666',
              verticalAlign: 'middle',
              marginLeft: '0.5em',
              width: '1em',
              height: '1em',
              background: this.state[key],
            }}
          />
        )}
      </label>
      <input
        className="form-control"
        id={key}
        value={this.state[key] || ''}
        onChange={(ev) => this.setState({ [key]: ev.target.value })}
      />
    </div>
  );

  renderDeleteButton = () => (
    <Button outline color="danger">
      <i className="fas fa-trash-alt mr-2" />
      Delete
    </Button>
  );

  render() {
    const { organization } = this.props;
    const { preferences } = organization;

    if (!preferences) {
      return (
        <div>
          There are no preferences yet.{' '}
          <Button onClick={this.uploadPreferences}>Create preferences now</Button>
        </div>
      );
    }

    return (
      <div>
        <div className="float-right" />
        <div className="clearfix" />
        <form>
          <div className="row">
            {this.renderInput({ title: 'Color Primary', key: 'colorPrimary', type: 'color' })}
            {this.renderInput({
              title: 'Color Text Primary',
              key: 'colorTextPrimary',
              type: 'color',
            })}
            {this.renderInput({
              title: 'Color Text Primary (hover)',
              key: 'colorTextPrimaryHover',
              type: 'color',
            })}
          </div>
          <div className="row">
            {this.renderInput({ title: 'Color Secondary', key: 'colorSecondary', type: 'color' })}
            {this.renderInput({
              title: 'Color Text Secondary',
              key: 'colorTextSecondary',
              type: 'color',
            })}
            {this.renderInput({
              title: 'Color Text Secondary (hover)',
              key: 'colorTextSecondaryHover',
              type: 'color',
            })}
          </div>
          <div className="row">
            {this.renderInput({ title: 'Color Tertiary', key: 'colorTertiary', type: 'color' })}
            {this.renderInput({ title: 'Border width', key: 'borderWidth' })}
          </div>
          <div className="row">
            {this.renderInput({ title: 'Title', key: 'title' })}
            {this.renderInput({ title: 'Short title', key: 'shortTitle' })}
            {this.renderCheck({ title: 'Show icon', key: 'showIcon' })}
          </div>

          <div className="row">{this.renderInput({ title: 'App override', key: 'app' })}</div>

          <div className="float-right">
            <ModalOrLink
              showModal={this.state.confirmDeletion}
              onHide={() => this.setState({ confirmDeletion: false })}
              onConfirmClicked={this.clearPreferences}
              confirmText="Delete"
              severity="danger"
              title={<span>Confirm your action</span>}
              onLinkClicked={() => this.setState({ confirmDeletion: true })}
              link={this.renderDeleteButton()}
            >
              <p>
                Do you want to delete preferences for <strong>{organization.name}</strong>?
              </p>
            </ModalOrLink>
            <Button outline color="secondary" onClick={this.resetPreferences} className="ml-1">
              Cancel
            </Button>
            <Button color="primary" onClick={this.uploadPreferences} className="ml-1">
              Update
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
  cookie: state.cookie,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  postOrganizationPreferences,
  cookieChooseOrganization,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrganizationPreferences);
