import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Link } from 'react-router-dom';

import C from '../../constants';

import { cookieChooseOrganization, cookieChooseOrganizationNone } from '../../actions/cookie';

class OrganizationChooser extends Component {
  handleMyOrganizationClick = (ev, organization) => {
    ev.preventDefault();
    this.props.cookieChooseOrganization(organization);
  };

  handleMyOrganizationDisable = (ev) => {
    ev.preventDefault();
    this.props.cookieChooseOrganizationNone();
  };

  renderOrganizationName = (organization) => {
    const { selected } = this.props.cookie.organization;

    if (selected && organization.id === selected.id) {
      return <strong className="font-big">{organization.name}</strong>;
    }

    return <span className="font-big text-secondary">{organization.name}</span>;
  };

  renderOrganizations = () => {
    const { myOrganizations } = this.props.organizations;
    const { selected } = this.props.cookie.organization;
    const authStatus = this.props.auth.status;

    if (authStatus !== C.AUTH_LOGGED_IN) {
      return null;
    }

    if (myOrganizations.status !== C.STATUS_SUCCESS) {
      return null;
    }

    if (myOrganizations.data.length === 0) {
      return null;
    }

    let hasActiveOrganizations = false;
    myOrganizations.data.forEach((organization) => {
      if (!organization.archived) hasActiveOrganizations = true;
    });
    if (!selected && !hasActiveOrganizations) return null;

    return (
      <div className="mt-3">
        <div className="d-flex align-items-center p-3">
          <h4 className="flex-grow-1 mb-0">Choose your organization</h4>
          <Link
            className="text-secondary"
            to="/organization/disable"
            onClick={(ev) => this.handleMyOrganizationDisable(ev)}
          >
            Disable
          </Link>
        </div>
        <ul className="list-group list-group-flush">
          {_.map(myOrganizations.data, (organization) => {
            if (organization.archived) return null;
            return (
              <li key={organization.id} className="list-group-item">
                <Link
                  to={`/organization/id/${organization.organizationId}`}
                  onClick={(ev) => this.handleMyOrganizationClick(ev, organization)}
                >
                  {this.renderOrganizationName(organization)}
                </Link>
                <Link
                  className="text-secondary float-right"
                  to={`/organization/${organization.organizationId}`}
                >
                  View
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  };

  render() {
    return this.renderOrganizations();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  organizations: state.organizations,
  cookie: state.cookie,
});

const mapDispatchToProps = { cookieChooseOrganization, cookieChooseOrganizationNone };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrganizationChooser);
