import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import { isAdmin } from '../actions/api';

class BrowsableEntity extends Component {
  constructor(props) {
    super(props);
    this.state = { search: '', page: 0, size: props.size, archived: false };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.id !== prevProps.id) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { search, size, page, archived } = this.state;
    this.props.find({ search, size, page, archived });
  };

  handleSearchTermChange = (search) => {
    this.setState({ search, page: 0 }, () => this.fetchData());
  };

  handlePaginationChange = (ev, page) => {
    ev.preventDefault();
    this.setState({ page }, () => this.fetchData());
  };

  handleArchivedChange = (ev, archived) => {
    ev.preventDefault();
    this.setState({ archived }, () => this.fetchData());
  };

  renderPaginationBar = () => {
    const { data } = this.props.entity;

    const { totalPages } = data;

    if (totalPages === 1) {
      return null;
    }

    return (
      <ul className="pagination">
        {_.map(_.range(0, totalPages), (index) => (
          <li className={this.state.page === index ? 'page-item active' : 'page-item'} key={index}>
            <a
              className="page-link"
              href={`page/${index + 1}`}
              onClick={(ev) => this.handlePaginationChange(ev, index)}
            >
              {index + 1}
            </a>
          </li>
        ))}
      </ul>
    );
  };

  renderSearchBar = () => {
    const { data } = this.props.entity;
    if (!data) {
      return null;
    }
    const { totalElements } = data;

    return (
      <div className="row">
        <div className="col-sm-4">
          <form>
            <div className="form-group">
              <input
                className="form-control"
                id="search"
                value={this.state.search}
                onChange={(ev) => this.handleSearchTermChange(ev.target.value)}
              />
              <div className="d-flex pt-1">
                <small>{totalElements} total</small>
                {this.props.allowArchived && (
                  <small className="ml-auto">
                    <a
                      href="/archived"
                      className="text-secondary"
                      onClick={(ev) => this.handleArchivedChange(ev, !this.state.archived)}
                    >
                      {`${this.state.archived ? 'hide archived...' : 'show archived...'}`}
                    </a>
                  </small>
                )}
              </div>
            </div>
          </form>
        </div>
        <div className="col-sm-4">{this.renderPaginationBar()}</div>
      </div>
    );
  };

  renderList = () => {
    const { data } = this.props.entity;
    if (!data) {
      return null;
    }

    return (
      <ul className="list-group">
        {_.map(data.content, (entity) => (
          <li key={entity.id} className="list-group-item">
            {this.props.renderItem(entity)}
          </li>
        ))}
      </ul>
    );
  };

  renderBrowse = () => (
    <div className="mt-3">
      {this.renderSearchBar()}
      {this.renderList()}
    </div>
  );

  render() {
    return (
      <div className={this.props.className}>
        {this.props.title && <h4>{this.props.title}</h4>}
        {this.renderBrowse()}
      </div>
    );
  }
}

BrowsableEntity.defaultProps = {
  id: 7,
  size: 20,
  onClick: (ev) => ev,
  title: '',
  find: ({ search }) => console.log(`Trying to find entities for search term: ${search}`),
  renderItem: (e) => `Data set ${e.id} (add renderItem prop)`,
  allowArchived: false,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  users: state.users,
});

const mapDispatchToProps = {
  isAdmin,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowsableEntity);
