import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const BrandToggle = (props) => (
  <div className="navbar-header">
    <button
      type="button"
      className="navbar-toggle collapsed"
      data-toggle="collapse"
      data-target="#bs-example-navbar-collapse-1"
      aria-expanded="false"
    >
      <span className="sr-only">Toggle navigation</span>
      <span className="icon-bar" />
      <span className="icon-bar" />
      <span className="icon-bar" />
    </button>
    <Link className="navbar-brand" id="navbar-brand" to="/">
      {props.children}
    </Link>
  </div>
);

BrandToggle.propTypes = {
  children: PropTypes.node,
};

BrandToggle.defaultProps = {
  children: 'Link',
};

export default BrandToggle;
