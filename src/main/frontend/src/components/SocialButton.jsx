import React from 'react';
import PropTypes from 'prop-types';

const SocialButton = (props) => (
  <button className={`btn btn-block btn-social btn-${props.provider}`} onClick={props.onClick}>
    <span className={`fab fa-${props.provider}`} />
    {props.children}
  </button>
);

SocialButton.propTypes = {
  provider: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node,
};

SocialButton.defaultProps = {
  children: '',
};

export default SocialButton;
