import React from 'react';
import { Cell } from 'fixed-data-table-2';

// used to be:
// {data.getObjectAt(rowIndex)[columnKey]}

class TextCell extends React.PureComponent {
  render() {
    const { data, rowIndex, columnKey, isLink, formId, ...props } = this.props;

    const ret = data[rowIndex + 1][columnKey];

    if (isLink) {
      if (ret === undefined || ret === null || ret === '') {
        return null;
      }

      const linkname = ret.split(/[\\/]/).pop();

      const scriptId = data[rowIndex + 1][columnKey + 1];
      const filename = linkname;
      const instanceId = data[rowIndex + 1][1];
      const processorLink = `/processor/${scriptId}?formId=${formId}&instanceId=${instanceId}&filename=${filename}`;

      return (
        <Cell {...props}>
          <a href={ret} target="_blank">
            {decodeURIComponent(linkname)}
          </a>{' '}
          <a href={processorLink} target="_blank">
            <i className="fa fa-external-link" />
          </a>
        </Cell>
      );
    }

    return <Cell {...props}>{data[rowIndex + 1][columnKey]}</Cell>;
  }
}
export default TextCell;
