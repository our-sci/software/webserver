import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import 'react-bootstrap-typeahead/css/Typeahead.css';
import { AsyncTypeahead, Menu, MenuItem, Highlighter } from 'react-bootstrap-typeahead';
import _ from 'lodash';

import axios from 'axios';
import * as Utils from '../utils';

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      options: [],
      selected: [],
      result: {},
    };
  }

  handleSearch = (query) => {
    this.setState({ isLoading: true });
    axios({
      url: `/api/find?search=${encodeURIComponent(query)}&size=${SearchBar.SIZE}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      this.setState({
        result: response.data,
        options: this.fillOptions(response.data),
        isLoading: false,
      });
    });
  };

  fillOptions = (data) => {
    const options = _.flatMap(data, (page, type) =>
      _.flatMap(page.content, (result) => ({ ...result, type }))
    );
    return options;
  };

  getLabelKey = (option) => {
    switch (option.type) {
      case 'surveys':
        return option.formTitle;
      case 'scripts':
        return option.name;
      case 'dashboards':
        return option.name;
      case 'organizations':
        return option.name;
      default:
        return option.label;
    }
  };

  reset = () => {
    this.setState({ selected: [] });
    this.typeahead.getInstance().clear();
    this.typeahead.getInstance().blur();
  };

  moreClick = (ev) => {
    ev.preventDefault();
    this.reset();
    // TODO: implementation
  };

  renderMenu = (results, menuProps) => {
    let idx = 0;
    const grouped = _.groupBy(results, (r) => r.type);
    const items = Object.keys(grouped)
      .sort()
      .map((type) => {
        const { totalElements } = this.state.result[type];
        return [
          !!idx && <Menu.Divider key={`${type}-divider`} />,
          <Menu.Header key={`${type}-header`}>{type}</Menu.Header>,
          _.map(grouped[type], (result) => {
            const item = (
              <MenuItem key={idx} option={result} position={idx}>
                <Highlighter search={menuProps.text}>{this.getLabelKey(result)}</Highlighter>
              </MenuItem>
            );

            idx++;
            return item;
          }),
          SearchBar.SHOW_MORE &&
            totalElements > SearchBar.SIZE && (
              <li className="dropdown-item text-right" key={`${type}-footer`}>
                <small>
                  <a href="/more" onClick={this.moreClick}>
                    {totalElements - SearchBar.SIZE} more
                  </a>
                </small>
              </li>
            ),
        ];
      });

    return <Menu {...menuProps}>{items}</Menu>;
  };

  renderMenuItemChildren = (option, props) => <div key={option.id}>{option.formTitle}</div>;

  onChange = (options) => {
    const option = options[0];

    if (!option) return;

    switch (option.type) {
      case 'surveys':
        this.props.history.push(Utils.createURLToSurvey(option));
        break;
      case 'scripts':
        this.props.history.push(Utils.createURLToScript(option));
        break;
      case 'dashboards':
        this.props.history.push(Utils.createURLToDashboard(option));
        break;
      case 'organizations':
        this.props.history.push(Utils.createURLToOrganization(option));
        break;
      default:
        break;
    }

    this.reset();
  };

  render() {
    return (
      <form className="os-search d-flex align-items-center">
        <AsyncTypeahead
          onChange={this.onChange}
          onSearch={this.handleSearch}
          isLoading={this.state.isLoading}
          options={this.state.options}
          renderMenu={this.renderMenu}
          labelKey={this.getLabelKey}
          filterBy={['name', 'formTitle', 'formId', 'scriptId', 'dashboardId', 'organizationId']}
          minLength={2}
          placeholder="Search..."
          autoComplete="off"
          type="text"
          className="w-100"
          selected={this.state.selected}
          ref={(typeahead) => {
            this.typeahead = typeahead;
          }}
        />

        <button
          className="btn btn-link os-search-docs-toggle d-md-none p-0 ml-3"
          type="button"
          data-toggle="collapse"
          data-target="#os-docs-nav"
          aria-controls="os-docs-nav"
          aria-expanded="false"
          aria-label="Toggle docs navigation"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 30 30"
            width="30"
            height="30"
            focusable="false"
          >
            <title>Menu</title>
            <path
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeMiterlimit="10"
              d="M4 7h22M4 15h22M4 23h22"
            />
          </svg>
        </button>
      </form>
    );
  }
}

SearchBar.SIZE = 6;
SearchBar.SHOW_MORE = false;

export default withRouter(SearchBar);
