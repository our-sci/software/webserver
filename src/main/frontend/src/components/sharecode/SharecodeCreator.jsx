import React, { Component } from 'react';
import { connect } from 'react-redux';

import ModalOrLink from '../ModalOrLink';

import { createSharecode } from '../../actions/api';
import { showFeedback } from '../../actions/utils';
import AutoCompleteEntity from '../dashboard/AutoCompleteEntity';

const INITAL_STATE = {
  showModal: false,
  email: '',
  sendmail: false,
  dashboard: null,
};

class SharecodeCreator extends Component {
  constructor(props) {
    super(props);
    this.state = INITAL_STATE;
  }

  submit = () => {
    const { email, sendmail, dashboard } = this.state;
    const { instanceIds } = this.props;

    const dashboardId = dashboard ? dashboard.dashboardId : '';

    console.log(`Creating share code for ${email} with ${instanceIds.length} survey results`);

    this.props.createSharecode({ email, instanceIds, dashboardId, sendmail }).then(() => {
      this.setState({ ...INITAL_STATE });
      this.props.showFeedback({
        title: 'Success!',
        body: `Share for ${instanceIds.length} surveys was created successfully.`,
      });
    });
  };

  render() {
    const { showModal, email, sendmail } = this.state;
    const { instanceIds } = this.props;

    return (
      <ModalOrLink
        showModal={showModal}
        onHide={() => this.setState({ ...INITAL_STATE })}
        title="Sharing is caring"
        onLinkClicked={() => this.setState({ showModal: true })}
        link="Share..."
        severity="danger"
        confirmText="CREATE SHARE"
        onConfirmClicked={this.submit}
        confirmActive={() => email !== '' && instanceIds.length !== 0}
      >
        <div className="mb-3">{`You are creating a share with ${instanceIds.length} surveys`}</div>
        <form>
          <div className="form-group">
            <label htmlFor="email">E-Mail</label>
            <input
              className="form-control"
              value={email}
              id="email"
              onChange={(ev) => this.setState({ email: ev.target.value })}
            />
            <small id="emailHelp" className="form-text text-muted">
              This email can be used to request a sharecode
            </small>
          </div>
          <AutoCompleteEntity
            type="dashboard"
            onSelect={(dashboard) => this.setState({ dashboard })}
          />
          <div className="form-check">
            <input
              type="checkbox"
              className="form-check-input"
              id="sendmail"
              checked={sendmail}
              onChange={(ev) => this.setState({ sendmail: ev.target.checked })}
            />
            <label className="form-check-label" htmlFor="sendmail">
              Also send sharecode to above e-mail address now
            </label>
          </div>
        </form>
        <br />
      </ModalOrLink>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = {
  createSharecode,
  showFeedback,
};

SharecodeCreator.defaultProps = {
  instanceIds: [],
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SharecodeCreator);
