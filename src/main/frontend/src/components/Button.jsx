import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ children, icon, type, onClick, style }) => {
  const buttonClass = `btn btn-${type}`;
  const iconClass = icon;

  return (
    <button type="button" className={buttonClass} onClick={onClick} style={style}>
      {icon !== '' && <span className={iconClass} aria-hidden="true" />} {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  icon: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  children: '',
  icon: '',
  type: 'primary',
  onClick: (ev) => ev,
};

export default Button;
