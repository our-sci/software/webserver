import C from '../constants';

export const clearSurveyResultCsv = (id) => (dispatch) => {
  dispatch({ type: C.CLEAR_SURVEY_RESULT_CSV, id });
};
