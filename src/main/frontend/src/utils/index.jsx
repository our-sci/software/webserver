import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

export const FOO = 2;

export const renderSurveyTitle = (title) => {
  const DELIMITER = ';';

  if (title.includes(DELIMITER)) {
    const mainTitle = title.substring(0, title.indexOf(DELIMITER));
    const subTitle = title.substring(title.indexOf(DELIMITER) + 1);
    return (
      <div>
        {mainTitle}
        <br />
        <small>{subTitle}</small>
      </div>
    );
  }

  return <div>{title}</div>;
};

const viewSurvey = (ev, survey, history) => {
  ev.preventDefault();
  history.push(`/survey/by-form-id/${survey.formId}`);
};

export const renderSurveyTitleWithLink = (survey, history) => {
  const DELIMITER = ';';

  const title = survey.formTitle;

  if (title.includes(DELIMITER)) {
    const mainTitle = title.substring(0, title.indexOf(DELIMITER));
    const subTitle = title.substring(title.indexOf(DELIMITER) + 1);
    return (
      <div>
        <a
          href={`/#/survey/by-form-id/${survey.formId}`}
          onClick={(ev) => viewSurvey(ev, survey, history)}
        >
          {mainTitle}
        </a>
        <br />
        <a
          href={`/#/survey/by-form-id/${survey.formId}`}
          onClick={(ev) => viewSurvey(ev, survey, history)}
        >
          <small>{subTitle}</small>
        </a>
      </div>
    );
  }

  return (
    <div>
      <a
        href={`/#/survey/by-form-id/${survey.formId}`}
        onClick={(ev) => viewSurvey(ev, survey, history)}
      >
        {title}
      </a>
    </div>
  );
};

export const clearStyles = (id) => {
  // clear
  const existing = document.getElementById(id);
  if (existing) {
    document.getElementById(id).outerHTML = '';
  }
};

export const appendStyles = (id, content) => {
  // clear
  clearStyles(id);

  // apply
  const s = document.createElement('style');
  s.id = id;
  s.type = 'text/css';
  s.innerHTML = content;
  document.head.appendChild(s);
};

export const getCurrentQuery = () => {
  const expr = /\?(.)*$/;
  const args = expr.exec(window.location.href);

  return args ? args[0] : '';
};

export const createURLToSurvey = (survey) => `/survey/by-form-id/${survey.formId}`;

export const createURLToScript = (script) => `/script/by-script-id/${script.scriptId}`;

export const createURLToDashboard = (dashboard) =>
  `/dashboard/by-dashboard-id/${dashboard.dashboardId}`;

export const createURLToOrganization = (organization) =>
  `/organization/${organization.organizationId}`;

export function createQueryString(params) {
  return _.join(_.map(_.pickBy(params, (v) => v), (v, k) => `${k}=${v}`), '&');
}

export const renderIcon = (icon, name) => {
  const dim = '4em';

  if (icon === null) {
    return (
      <div
        style={{
          width: dim,
          height: dim,
          border: '1px solid #999',
          textAlign: 'center',
          lineHeight: dim,
        }}
      >
        <span style={{ fontSize: '1.25em', textTransform: 'uppercase' }}>{name.substr(0, 1)}</span>
      </div>
    );
  }

  return (
    <div style={{ width: dim, height: dim, border: '1px solid #999' }}>
      <img src={icon} style={{ width: dim, height: dim }} alt="icon" />
    </div>
  );
};

export const renderItemDashboard = ({ item, query }) => {
  const { dashboardId, name } = item;
  const queryString = createQueryString(query);

  if (!queryString) {
    return <Link to={`/dashboard/by-dashboard-id/${dashboardId}`}>{name}</Link>;
  }
  return <Link to={`/dashboard/by-dashboard-id/${dashboardId}?${queryString}`}>{name}</Link>;
};

export const renderItemSurvey = ({ item }) => {
  const { formId, formTitle } = item;
  return <Link to={`/survey/by-form-id/${formId}`}>{formTitle}</Link>;
};

export const renderItemContribution = ({ item }) => {
  const { survey, total } = item;
  const contributions = (
    <small>
      {total} {`${total > 1 ? 'contributions' : 'contribution'}`}
    </small>
  );
  return (
    <div>
      <Link to={`/survey/by-form-id/${survey.formId}`}>{survey.formTitle}</Link>
      <br />
      {contributions}
    </div>
  );
};

export const singularOrPlural = (singular, size, specialPlural = null) => {
  if (size === 1) {
    return singular;
  }

  if (specialPlural) {
    return specialPlural;
  }

  return `${singular}s`;
};
