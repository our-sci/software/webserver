import C from '../constants';

const INITIAL_STATE = {};

export default (state, action) => {
  const { uid } = action;
  switch (action.type) {
    case C.API_CONTRIBUTION_USER_LOADING:
      return { ...state, [uid]: { data: null, status: C.STATUS_LOADING } };
    case C.API_CONTRIBUTION_USER_SUCCESS:
      return { ...state, [uid]: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_CONTRIBUTION_USER_ERROR:
      return { ...state, [uid]: { data: action.payload, status: C.STATUS_ERROR } };

    default:
      return state || INITIAL_STATE;
  }
};
