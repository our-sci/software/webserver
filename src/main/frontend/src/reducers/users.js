import C from '../constants';

const INITIAL_STATE = {
  find: {
    data: null,
    status: C.STATUS_LOADING,
  },
};

export default (state, action) => {
  switch (action.type) {
    case C.API_USERS_FIND_LOADING:
      return { ...state, find: { data: null, status: C.STATUS_LOADING } };
    case C.API_USERS_FIND_SUCCESS:
      return { ...state, find: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_USERS_FIND_ERROR:
      return { ...state, find: { data: action.payload, status: C.STATUS_ERROR } };
    default:
      return state || INITIAL_STATE;
  }
};
