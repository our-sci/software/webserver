import { combineReducers } from 'redux';

import auth from './auth';
import feedback from './feedback';
import client from './api_client';
import admin from './api_admin';
import scripts from './scripts';
import dashboards from './dashboards';
import surveyResults from './surveyResults';
import organizations from './organization';
import surveys from './surveys';
import cookie from './cookie';
import contributions from './contributions';
import invitation from './invitation';
import tpcredential from './tpcredential';
import users from './users';

const rootReducer = combineReducers({
  auth,
  feedback,
  api: combineReducers({ client, admin }),
  scripts,
  dashboards,
  surveyResults,
  organizations,
  surveys,
  cookie,
  contributions,
  invitation,
  tpcredential,
  users,
});

export default rootReducer;
