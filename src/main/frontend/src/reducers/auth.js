import C from '../constants';

const INITIAL_STATE = {
  username: null,
  uid: null,
  status: C.AUTH_ANONYMOUS,
  email: '',
  password: '',
  token: null,
};

export default (state, action) => {
  switch (action.type) {
    case C.AUTH_OPEN:
      return {
        ...state,
        status: C.AUTH_AWAITING_RESPONSE,
        username: 'guest',
        uid: null,
      };
    case C.AUTH_LOGIN:
      return {
        ...state,
        status: C.AUTH_LOGGED_IN,
        username: action.username,
        uid: action.uid,
      };
    case C.AUTH_LOGOUT:
      return {
        ...state,
        status: C.AUTH_ANONYMOUS,
        username: 'guest',
        uid: null,
        token: null,
      };
    case C.AUTH_EMAIL_CHANGED:
      return {
        ...state,
        email: action.payload,
      };
    case C.AUTH_PASSWORD_CHANGED:
      return {
        ...state,
        password: action.payload,
      };
    case C.AUTH_TOKEN_CHANGED:
      return {
        ...state,
        token: action.payload,
      };
    default:
      return state || INITIAL_STATE;
  }
};
