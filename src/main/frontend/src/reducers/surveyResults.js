import C from '../constants';

const INITIAL_STATE = {};

export default (state, action) => {
  const formId = action.id;

  switch (action.type) {
    case C.API_SURVEY_RESULT_CSV_LOADING: {
      return { ...state, [formId]: { data: null, status: C.STATUS_LOADING } };
    }
    case C.API_SURVEY_RESULT_CSV_SUCCESS: {
      return {
        ...state,
        [formId]: { data: action.payload, status: C.STATUS_SUCCESS },
      };
    }
    case C.API_SURVEY_RESULT_CSV_ERROR: {
      return { ...state, [formId]: { data: action.payload, status: C.STATUS_ERROR } };
    }
    case C.CLEAR_SURVEY_RESULT_CSV: {
      return { ...INITIAL_STATE };
    }

    default:
      return state || INITIAL_STATE;
  }
};
