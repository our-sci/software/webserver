import C from '../constants';

const INITIAL_STATE = {
  list: {
    data: null,
    status: C.STATUS_LOADING,
  },
  find: {
    data: null,
    status: C.STATUS_LOADING,
  },
  survey: {
    data: null,
    status: C.STATUS_LOADING,
  },
  lastCreated: {
    data: null,
    status: C.STATUS_LOADING,
  },
  latestContributions: {
    data: null,
    status: C.STATUS_LOADING,
  },
  archived: {
    data: null,
    status: C.STATUS_LOADING,
  },
  organization: {
    data: null,
    status: C.STATUS_LOADING,
  },
  user: {},
};

export default (state, action) => {
  switch (action.type) {
    case C.API_SURVEY_LIST_LOADING:
      return { ...state, list: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_LIST_SUCCESS:
      return { ...state, list: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_LIST_ERROR:
      return { ...state, list: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SURVEY_FIND_LOADING:
      return { ...state, find: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_FIND_SUCCESS:
      return { ...state, find: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_FIND_ERROR:
      return { ...state, find: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SURVEY_USER_LOADING:
      return {
        ...state,
        user: { ...state.user, [action.id]: { data: null, status: C.STATUS_LOADING } },
      };
    case C.API_SURVEY_USER_SUCCESS:
      return {
        ...state,
        user: { ...state.user, [action.id]: { data: action.payload, status: C.STATUS_SUCCESS } },
      };
    case C.API_SURVEY_USER_ERROR:
      return {
        ...state,
        user: { ...state.user, [action.id]: { data: action.payload, status: C.STATUS_ERROR } },
      };

    case C.API_SURVEY_LASTCREATED_LOADING:
      return { ...state, lastCreated: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_LASTCREATED_SUCCESS:
      return { ...state, lastCreated: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_LASTCREATED_ERROR:
      return { ...state, lastCreated: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SURVEY_ARCHIVED_LOADING:
      return { ...state, archived: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_ARCHIVED_SUCCESS:
      return { ...state, archived: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_ARCHIVED_ERROR:
      return { ...state, archived: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SURVEY_LATESTCONTRIBUTIONS_LOADING:
      return { ...state, latestContributions: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_LATESTCONTRIBUTIONS_SUCCESS:
      return { ...state, latestContributions: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_LATESTCONTRIBUTIONS_ERROR:
      return { ...state, latestContributions: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SURVEY_ORGANIZATION_LOADING:
      return { ...state, organization: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_ORGANIZATION_SUCCESS:
      return { ...state, organization: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_ORGANIZATION_ERROR:
      return { ...state, organization: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SURVEY_ID_LOADING:
      return { ...state, survey: { data: null, status: C.STATUS_LOADING } };
    case C.API_SURVEY_ID_SUCCESS:
      return { ...state, survey: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SURVEY_ID_ERROR:
      return { ...state, survey: { data: action.payload, status: C.STATUS_ERROR } };

    default:
      return state || INITIAL_STATE;
  }
};
