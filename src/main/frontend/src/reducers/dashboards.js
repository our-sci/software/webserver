import C from '../constants';

const INITIAL_STATE = {
  list: {
    data: null,
    status: C.STATUS_LOADING,
  },
  dashboard: {
    data: null,
    status: C.STATUS_LOADING,
  },
  organization: {
    data: null,
    status: C.STATUS_LOADING,
  },
  find: {
    data: null,
    status: C.STATUS_LOADING,
  },
};

export default (state, action) => {
  switch (action.type) {
    case C.API_DASHBOARD_LIST_LOADING:
      return { ...state, list: { data: null, status: C.STATUS_LOADING } };
    case C.API_DASHBOARD_LIST_SUCCESS:
      return { ...state, list: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_DASHBOARD_LIST_ERROR:
      return { ...state, list: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_DASHBOARD_ID_LOADING:
      return { ...state, dashboard: { data: null, status: C.STATUS_LOADING } };
    case C.API_DASHBOARD_ID_SUCCESS:
      return { ...state, dashboard: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_DASHBOARD_ID_ERROR:
      return { ...state, dashboard: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_DASHBOARD_ORGANIZATION_LOADING:
      return { ...state, organization: { data: null, status: C.STATUS_LOADING } };
    case C.API_DASHBOARD_ORGANIZATION_SUCCESS:
      return { ...state, organization: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_DASHBOARD_ORGANIZATION_ERROR:
      return { ...state, organization: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_DASHBOARD_FIND_LOADING:
      return { ...state, find: { data: null, status: C.STATUS_LOADING } };
    case C.API_DASHBOARD_FIND_SUCCESS:
      return { ...state, find: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_DASHBOARD_FIND_ERROR:
      return { ...state, find: { data: action.payload, status: C.STATUS_ERROR } };

    default:
      return state || INITIAL_STATE;
  }
};
