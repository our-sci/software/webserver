import firebase from './firebase';
import auth from './auth';
import feedback from './feedback';
import api from './api';
import status from './status';
import date from './date';
import cookie from './cookie';
import invitation from './invitation';

export default {
  ...firebase,
  ...auth,
  ...feedback,
  ...api,
  ...status,
  ...date,
  ...cookie,
  ...invitation,
};
