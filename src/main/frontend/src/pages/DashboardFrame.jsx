import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { getDashboardByDashboardId, getDashboardByDatabaseId } from '../actions/api';

import { clearStyles, appendStyles, getCurrentQuery } from '../utils';

const STYLES = `
  @media (min-width: 768px) {
    .os-content {
      min-height: 100vh !important;
      flex: 1 0 !important;
      max-width: 100%;
      padding-top: 0px !important;
    }
    .navbar-brand {
      display: block !important;
    }
  }
  .navbar-brand { display: none }
`;

const STYLE_ID = 'styles-dashboard';

class DashboardSingle extends Component {
  componentWillMount() {
    appendStyles(STYLE_ID, STYLES);
  }

  componentDidMount() {
    const { dashboardId, databaseId } = this.props.match.params;
    if (dashboardId) {
      this.props.getDashboardByDashboardId(dashboardId);
    } else {
      this.props.getDashboardByDatabaseId(databaseId);
    }
  }

  componentWillUnmount() {
    clearStyles(STYLE_ID);
  }

  renderFrame = () => {
    const { dashboardId } = this.props.match.params;
    let url = `/dashboard/${dashboardId}`;
    if (process.env.NODE_ENV === 'development') {
      // proxy does not work with html pages
      url = `http://localhost:5000/dashboard/${dashboardId}`;
      if (process.env.REACT_APP_IP) {
        url = `http://${process.env.REACT_APP_IP}:5000/dashboard/${dashboardId}`;
      }
    }

    url += getCurrentQuery();

    return <iframe id="dashboard" title="dashboard" src={url} className="dashboard-iframe" />;
  };

  render() {
    return this.renderFrame();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  dashboards: state.dashboards,
});

const mapDispatchToProps = {
  getDashboardByDashboardId,
  getDashboardByDatabaseId,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashboardSingle)
);
