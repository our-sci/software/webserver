import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import _ from 'lodash';
import ReactMarkdown from 'react-markdown';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import C from '../constants';
import {
  getOrganizationById,
  removeOrganizationMember,
  findMyOrganizations,
  isAdmin,
} from '../actions/api';
import { cookieChooseOrganizationNone } from '../actions/cookie';
import withCustomMarkdownRenderer from '../components/withCustomMarkdownRenderer';

const CustomMarkdown = withCustomMarkdownRenderer(ReactMarkdown);

class OrganizationSingle extends Component {
  constructor(props) {
    super(props);
    this.renderOrganization = this.renderOrganization.bind(this);

    this.state = { confirmLeaving: false };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.organizationId !== this.props.match.params.organizationId) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { organizationId } = this.props.match.params;
    this.props.getOrganizationById(organizationId);
  };

  handleLeaving = (id, organizationId) => {
    console.log(`leaving this organization for realz with id ${id}`);
    this.props
      .removeOrganizationMember(id, organizationId)
      .then(() => this.props.findMyOrganizations())
      .then(() => {
        this.props.cookieChooseOrganizationNone();
        this.navigateBack();
      });

    this.setState({ confirmLeaving: false });
  };

  cancelLeaving = () => {
    this.setState({ confirmLeaving: false });
  };

  navigateBack = () => {
    this.props.history.push('/organization');
  };

  renderMarkdown = () => {
    const { current } = this.props.organizations;

    return (
      <CustomMarkdown
        skipHtml={false}
        escapeHtml={false}
        source={current.data.content}
        renderers={{ image: this.Image }}
      />
    );
  };

  renderMembers = () => {
    const { current } = this.props.organizations;
    const organization = current.data;
    return (
      <div>
        Organization <strong>{organization.name}</strong> has {organization.members.length} members.
      </div>
    );
  };

  renderLeaveButton = () => {
    const { current } = this.props.organizations;
    const organization = current.data;

    const { uid } = this.props.auth;

    const isCreator = organization.creator.username === uid;
    if (isCreator) {
      return null;
    }
    let isMember = false;
    let memberDatabaseId = -1;

    _.map(organization.members, (member) => {
      if (member.user.username === uid) {
        isMember = true;
        memberDatabaseId = member.id;
      }
    });

    if (isMember) {
      if (this.state.confirmLeaving) {
        return (
          <Modal
            isOpen={this.state.confirmLeaving}
            onClosed={this.cancelLeaving}
            toggle={this.cancelLeaving}
          >
            <ModalHeader toggle={this.cancelLeaving}>Confirm your action</ModalHeader>
            <ModalBody>
              Do you want to leave organization <strong>{organization.name}</strong>?
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={this.cancelLeaving}>
                Cancel
              </Button>
              <Button
                color="danger"
                onClick={() => this.handleLeaving(memberDatabaseId, organization.organizationId)}
              >
                Leave
              </Button>
            </ModalFooter>
          </Modal>
        );
      }

      return (
        <Button outline color="warning" onClick={() => this.setState({ confirmLeaving: true })}>
          <i className="fas fa-running" /> Leave
        </Button>
      );
    }

    return null;
  };

  renderEditButton = () => {
    const { current } = this.props.organizations;
    const organization = current.data;

    const { uid } = this.props.auth;

    let organizationAdmin = false;

    _.map(organization.members, (member) => {
      if (member.user.username === uid && member.admin) {
        organizationAdmin = true;
      }
    });

    if (organizationAdmin || this.props.isAdmin()) {
      return (
        <Link to={`/organization/${organization.organizationId}/edit`}>
          <Button outline className="ml-1">
            <i className="fas fa-edit" /> Edit
          </Button>
        </Link>
      );
    }

    return null;
  };

  renderTitle = () => {
    const { current } = this.props.organizations;
    const organization = current.data;

    return (
      <h2>
        {organization.name}
        <br />
        <small>Currently {organization.members.length} members</small>
        <br />
        <small>
          <small>{organization.organizationId}</small>
        </small>
      </h2>
    );
  };

  renderOrganization() {
    const { current } = this.props.organizations;

    if (current.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (current.status === C.STATUS_ERROR) {
      return <div>Error: {current.data}</div>;
    }

    return (
      <div>
        <div className="btn-toolbar float-right">
          {this.renderLeaveButton()}
          {this.renderEditButton()}
        </div>
        {this.renderMarkdown()}
      </div>
    );
  }

  render() {
    return this.renderOrganization();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  removeOrganizationMember,
  findMyOrganizations,
  cookieChooseOrganizationNone,
  isAdmin,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(OrganizationSingle)
);
