import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { sendSharecodeEmail } from '../actions/api';
import { showFeedback } from '../actions/utils';

class Share extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', error: false, success: false };
  }

  submitForm = (ev) => {
    ev.preventDefault();
    const { email } = this.state;
    this.props
      .sendSharecodeEmail(email)
      .then((response) => this.setState({ error: false, success: true }))
      .catch(() => this.setState({ error: true, success: false }));
  };

  renderError = () => {
    const { email, error } = this.state;
    if (!error) return null;
    return <div className="alert alert-danger">No shares found for email {email}</div>;
  };

  renderSuccess = () => {
    const { email, success } = this.state;
    if (!success) return null;
    return (
      <div className="alert alert-success">
        Success! An email with your personal share link has been sent to <strong>{email}</strong>
      </div>
    );
  };

  render() {
    const { email } = this.state;
    return (
      <div style={{ maxWidth: '30rem' }}>
        <h2>Request your share link</h2>
        If someone has been contributing in your stead, you can request a share link here. You will
        then receive an email with a secret link to your contribution listings.
        <form onSubmit={(ev) => this.submitForm(ev)} className="clearfix my-3">
          <div className="form-group">
            <label htmlFor="user-email">Email</label>
            <input
              className="form-control"
              id="user-email"
              value={email}
              onChange={(ev) => this.setState({ email: ev.target.value })}
            />
          </div>
          <button type="submit" className="btn btn-primary float-right" disabled={email === ''}>
            Submit
          </button>
          <input
            type="reset"
            className="btn btn-outline-secondary mr-2 float-right"
            onClick={(ev) => this.setState({ email: '', success: false, error: false })}
            value="Reset"
          />
        </form>
        {this.renderError()}
        {this.renderSuccess()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  tpcredential: state.tpcredential,
  dashboards: state.dashboards,
});

const mapDispatchToProps = {
  sendSharecodeEmail,
  showFeedback,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Share)
);
