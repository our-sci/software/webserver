import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import TpCredentialWidget from '../components/TpCredentialWidget';

class TpCredentialAdd extends Component {
  constructor(props) {
    super(props);

    const { uid } = this.props.match.params;

    this.state = {
      id: null,
      uid,
    };
  }

  render() {
    const { id, uid } = this.state;
    return <TpCredentialWidget mode="ADD" id={id} uid={uid} />;
  }
}

export default withRouter(TpCredentialAdd);
