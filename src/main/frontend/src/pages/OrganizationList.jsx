import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import { findOrganizations, findMyOrganizations } from '../actions/api';
import C from '../constants';
import CreateButton from '../components/CreateButton';

import BrowsableEntity from '../components/BrowsableEntity';

class OrganizationList extends Component {
  componentDidMount() {
    this.props.findMyOrganizations();
  }

  renderIcon = (organization) => {
    const dim = '4em';

    const { icon } = organization;
    if (icon === null) {
      return (
        <div
          style={{
            width: dim,
            height: dim,
            border: '1px solid #BBB',
            textAlign: 'center',
            lineHeight: dim,
          }}
        >
          <span style={{ fontSize: '1.25em', textTransform: 'uppercase' }}>
            {organization.name.substr(0, 1)}
          </span>
        </div>
      );
    }

    return (
      <img src={organization.icon} style={{ width: dim, height: dim }} alt="organization icon" />
    );
  };

  renderEntity = (e) => {
    const { organizationId, name, description, archived } = e;
    return (
      <div className={`d-flex ${archived ? 'archived' : ''}`}>
        {this.renderIcon(e)}
        <Link to={`/organization/${organizationId}`} className="ml-3 text-black">
          {name}
          <br />
          <span className="text-secondary">{description}</span>
        </Link>
      </div>
    );
  };

  renderOrganizations = (title, organizations) => {
    if (organizations.status !== C.STATUS_SUCCESS) {
      return null;
    }

    if (organizations.data.length === 0) {
      return null;
    }

    return (
      <div className="mt-4 mb-5">
        <h4>{title}</h4>
        <ul className="list-group mt-3">
          {_.map(organizations.data, (entity) => (
            <li key={entity.id} className="list-group-item">
              {this.renderEntity(entity)}
            </li>
          ))}
        </ul>
      </div>
    );
  };

  render() {
    const mine = this.props.organizations.myOrganizations;
    const authStatus = this.props.auth.status;

    return (
      <div>
        <div className="btn-toolbar float-right">
          <CreateButton to="/organization/create">Create Organization</CreateButton>
        </div>
        <h2>Organizations</h2>
        {authStatus === C.AUTH_LOGGED_IN && this.renderOrganizations('My organizations', mine)}
        <BrowsableEntity
          className="mt-3"
          title="Browse Organizations"
          entity={this.props.organizations.find}
          find={this.props.findOrganizations}
          renderItem={this.renderEntity}
          allowArchived
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  findOrganizations,
  findMyOrganizations,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrganizationList);
