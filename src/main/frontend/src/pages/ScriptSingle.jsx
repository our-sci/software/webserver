import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

import C from '../constants';
import { getScriptByScriptId, getScriptByDatabaseId, isAdmin } from '../actions/api';
import DownloadButton from '../components/DownloadButton';
import Toggle from '../components/Toggle';

class ScriptSingle extends Component {
  constructor(props) {
    super(props);
    this.renderScript = this.renderScript.bind(this);
    this.renderTable = this.renderTable.bind(this);
    this.toggleArchived = this.toggleArchived.bind(this);

    this.state = { keys: [], results: [] };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.scriptId !== this.props.match.params.scriptId) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { scriptId, databaseId } = this.props.match.params;
    if (scriptId) {
      this.props.getScriptByScriptId(scriptId);
    } else {
      this.props.getScriptByDatabaseId(databaseId);
    }
  };

  hasAdminRights = (script) => {
    const { uid } = this.props.auth;

    if (uid === script.data.user.username) {
      return true;
    }

    if (this.props.isAdmin()) {
      return true;
    }

    return false;
  };

  toggleArchived() {
    console.log('toggling');
    const { scriptId } = this.props.match.params;

    const requestUrl = `/api/script/toggle/archived/by-script-id/${scriptId}`;

    axios({
      url: requestUrl,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(() => {
        this.props.getScriptByScriptId(scriptId);
      })
      .catch(() => {
        console.log('error');
      });
  }

  renderTable() {
    const { script } = this.props.scripts;
    console.log(script.data);

    return (
      <table className="table">
        <thead>
          <tr>
            <th>Key</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>id</td>
            <td>{script.data.scriptId}</td>
          </tr>
          <tr>
            <td>name</td>
            <td>{script.data.name}</td>
          </tr>
          <tr>
            <td>description</td>
            <td>{script.data.description}</td>
          </tr>
          <tr>
            <td>version</td>
            <td>{script.data.version}</td>
          </tr>
        </tbody>
      </table>
    );
  }

  renderScript() {
    const { script } = this.props.scripts;

    if (script.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (script.status === C.STATUS_ERROR) {
      return <div>Error: {script.data}</div>;
    }

    const { scriptId } = this.props.match.params;

    return (
      <div>
        <div className="btn-toolbar float-right">
          {this.hasAdminRights(script) && (
            <Toggle
              checked={script.data.archived}
              onToggle={this.toggleArchived}
              textChecked="RESTORE"
              textUnchecked="ARCHIVE"
              className="mr-1"
            />
          )}
          <DownloadButton href={`/api/script/download/zip/by-script-id/${scriptId}`}>
            Script as ZIP
          </DownloadButton>
        </div>
        <h2>
          {script.data.name}
          <br />
          <small>{script.data.scriptId}</small>
        </h2>
        {this.renderTable()}
      </div>
    );
  }

  render() {
    return this.renderScript();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
});

const mapDispatchToProps = {
  getScriptByScriptId,
  getScriptByDatabaseId,
  isAdmin,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ScriptSingle)
);
