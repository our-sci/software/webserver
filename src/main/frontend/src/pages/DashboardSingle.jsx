import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

import C from '../constants';
import { getDashboardByDashboardId, getDashboardByDatabaseId, isAdmin } from '../actions/api';
import Toggle from '../components/Toggle';

import { getCurrentQuery } from '../utils';

class DashboardSingle extends Component {
  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.dashboardId !== this.props.match.params.dashboardId) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { dashboardId, databaseId } = this.props.match.params;
    if (dashboardId) {
      this.props.getDashboardByDashboardId(dashboardId);
    } else {
      this.props.getDashboardByDatabaseId(databaseId);
    }
  };

  hasAdminRights = (dashboard) => {
    const { uid } = this.props.auth;

    if (uid === dashboard.data.user.username) {
      return true;
    }

    if (this.props.isAdmin()) {
      return true;
    }

    return false;
  };

  toggleArchived = () => {
    const { dashboardId } = this.props.match.params;

    const requestUrl = `/api/dashboard/toggle/archived/by-dashboard-id/${dashboardId}`;

    axios({
      url: requestUrl,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(() => {
        this.props.getDashboardByDashboardId(dashboardId);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  renderControls = (dashboard) => {
    const { dashboardId } = dashboard.data;

    return (
      <div className="dropleft float-right" style={{ bottom: '0.5rem' }}>
        <button
          className="btn btn-outline-secondary"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          More...
        </button>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a href={`/dashboard/${dashboardId}/${getCurrentQuery()}`} className="dropdown-item">
            <i className="fas fa-external-link-alt mr-1" />
            Open
          </a>
          <a
            href={`/api/dashboard/download/zip/by-dashboard-id/${dashboardId}`}
            className="dropdown-item"
          >
            <i className="fas fa-file-archive mr-2" />
            Download
          </a>
          {this.hasAdminRights(dashboard) && (
            <Toggle
              checked={dashboard.data.archived}
              onToggle={this.toggleArchived}
              textChecked="Restore"
              textUnchecked="Archive"
              className="dropdown-item"
            />
          )}
        </div>
      </div>
    );
  };

  renderDashboard = () => {
    const { dashboard } = this.props.dashboards;

    if (dashboard.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (dashboard.status === C.STATUS_ERROR) {
      return <div>Error: {dashboard.data}</div>;
    }

    return (
      <div className="clearfix">
        {this.renderControls(dashboard)}

        <h4>{dashboard.data.name}</h4>
      </div>
    );
  };

  render() {
    return this.renderDashboard();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  dashboards: state.dashboards,
});

const mapDispatchToProps = {
  getDashboardByDashboardId,
  getDashboardByDatabaseId,
  isAdmin,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashboardSingle)
);
