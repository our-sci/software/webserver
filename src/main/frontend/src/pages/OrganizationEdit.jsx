import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Button } from 'reactstrap';

import C from '../constants';
import { getOrganizationById, archiveOrganization } from '../actions/api';
import { basename } from '../actions/utils';

import Description from '../components/organization/Description';
import Members from '../components/organization/Members';
import Favorites from '../components/organization/Favorites';
import Assets from '../components/organization/Assets';

import Spacer from '../components/Spacer';
import Tabs from '../components/Tabs';
import Tab from '../components/Tab';
import Preferences from '../components/organization/Preferences';
import Invitations from '../components/organization/Invitations';

class OrganizationEdit extends Component {
  constructor(props) {
    super(props);
    this.renderOrganization = this.renderOrganization.bind(this);

    this.state = {
      error: null,
      activeTab: 0,
    };
  }

  componentDidMount() {
    const tail = basename(this.props.match.url);

    let activeTab = 0;
    switch (tail) {
      case 'edit-members':
        activeTab = 1;
        break;
      case 'edit-invitations':
        activeTab = 2;
        break;
      case 'edit-favorites':
        activeTab = 3;
        break;
      case 'edit-assets':
        activeTab = 4;
        break;
      case 'edit-preferences':
        activeTab = 5;
        break;
      default:
        activeTab = 0;
    }

    this.setState({ activeTab });

    const { organizationId } = this.props.match.params;
    this.props.getOrganizationById(organizationId);
  }

  getOrganization = () => {
    const { current } = this.props.organizations;
    const organization = current.data;
    return organization;
  };

  navigateBack = () => {
    const { organizationId } = this.props.match.params;
    this.props.history.push(`/organization/${organizationId}`);
  };

  updateOrganizationSetArchived = (archived) => {
    const { organizationId } = this.props.match.params;
    this.props
      .archiveOrganization(organizationId, archived)
      .then(() => this.props.getOrganizationById(organizationId));
  };

  renderArchiveButton = () => {
    const { archived } = this.getOrganization();
    if (archived) {
      return (
        <Button
          icon="glyphicon glyphicon-trash"
          color="warning"
          onClick={() => this.updateOrganizationSetArchived(false)}
        >
          Restore
        </Button>
      );
    }

    return (
      <Button color="danger" onClick={() => this.updateOrganizationSetArchived(true)}>
        <i className="fas fa-trash" /> Archive
      </Button>
    );
  };

  renderError = () => {
    const { error } = this.state;

    if (!error) {
      return null;
    }

    return (
      <div className="alert alert-warning">
        <strong>{error.data.message.key}</strong> {error.data.message.description}
      </div>
    );
  };

  renderTabs = () => {
    const { organizationId } = this.props.match.params;

    return (
      <Tabs
        activeTab={this.state.activeTab}
        onSelect={(index) => this.setState({ activeTab: index })}
      >
        <Tab title="Description" link={`/organization/${organizationId}/edit`}>
          <Description organization={this.getOrganization()} />
        </Tab>
        <Tab title="Members" link={`/organization/${organizationId}/edit-members`}>
          <Members organization={this.getOrganization()} />
        </Tab>
        <Tab title="Invitations" link={`/organization/${organizationId}/edit-invitations`}>
          <Invitations organization={this.getOrganization()} />
        </Tab>
        <Tab title="Favorites" link={`/organization/${organizationId}/edit-favorites`}>
          <Favorites organization={this.getOrganization()} />
        </Tab>
        <Tab title="Assets" link={`/organization/${organizationId}/edit-assets`}>
          <Assets organizationId={this.props.match.params.organizationId} />
        </Tab>
        <Tab title="Preferences" link={`/organization/${organizationId}/edit-preferences`}>
          <Preferences organization={this.getOrganization()} />
        </Tab>
      </Tabs>
    );
  };

  renderOrganization() {
    const { current } = this.props.organizations;

    if (current.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (current.status === C.STATUS_ERROR) {
      return <div>Error: {current.data}</div>;
    }

    return (
      <div>
        {this.renderError()}
        <div className="btn-toolbar float-right">{this.renderArchiveButton()}</div>
        <h3>
          Edit Organization
          <br />
          <small>{current.data.organizationId}</small>
        </h3>
        <Spacer height={10} />
        {this.renderTabs()}
      </div>
    );
  }

  render() {
    return this.renderOrganization();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  archiveOrganization,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(OrganizationEdit)
);
