import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  getOrganizationById,
  addOrganizationMember,
  removeOrganizationMember,
  updateOrganizationMember,
  getTpCredential,
  postTpCredential,
} from '../actions/api';

import TpCredentialWidget from '../components/TpCredentialWidget';

class TpCredentialEdit extends Component {
  constructor(props) {
    super(props);

    const { id } = this.props.match.params;

    this.state = {
      id,
      uid: null,
    };
  }

  render() {
    const { id, uid } = this.state;
    return <TpCredentialWidget mode="EDIT" id={id} uid={uid} />;
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  addOrganizationMember,
  removeOrganizationMember,
  updateOrganizationMember,
  getTpCredential,
  postTpCredential,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TpCredentialEdit)
);
