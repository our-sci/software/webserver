import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import _ from 'lodash';

class MobileApp extends Component {
  constructor(props) {
    super(props);
    this.state = { surveys: [] };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    axios.get('/api/survey/find').then((response) => {
      this.setState({ surveys: response.data.content });
      console.log(response.data.content);
    });
  };

  renderSurvey = (survey) => (
    <Link
      className="p-2 d-block"
      style={{ borderBottom: '1px solid #ddd', textDecoration: 'none', color: '#000' }}
      to={`/survey/by-form-id/${survey.formId}`}
    >
      <div>{survey.formTitle}</div>
      <div className="text-muted" style={{ fontSize: '80%' }}>
        updated {moment(survey.date).fromNow()}
      </div>
    </Link>
  );

  render() {
    const { selected } = this.props.cookie.organization;
    let appLink = 'https://play.google.com/store/apps/details?id=org.oursci.android.ourscikit';
    if (selected && selected.preferences) {
      if (selected.preferences.app && selected.preferences.app !== '') {
        appLink = selected.preferences.app;
      }
    }

    return (
      <div style={{ maxWidth: '60rem' }}>
        <div className="d-flex flex-wrap justify-content-around">
          <div style={{ maxWidth: '30rem' }} className="p-2 pb-4">
            <h1>Our-Sci App</h1>
            <p>
              Use the <a href="https://gitlab.com/our-sci/android">open-source</a> mobile
              application to fill out surveys, connect to external devices, collect samples, and{' '}
              upload your results to the Our-Sci platform.
            </p>
            <a href={appLink} target="_blank" rel="noopener noreferrer">
              Download from Google Play Store
            </a>
          </div>
          <div style={{ position: 'relative' }}>
            <img
              src="android-screenshot-surveylist-sm.png"
              style={{ height: '40rem' }}
              alt="Mobile App Screenshot"
            />
            <div
              className="showroom-inner"
              style={{
                width: '283px',
                height: '413px',
                position: 'absolute',
                top: '117px',
                left: '38px',
              }}
            >
              {_.map(this.state.surveys, (survey) => this.renderSurvey(survey))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cookie: state.cookie,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MobileApp);
