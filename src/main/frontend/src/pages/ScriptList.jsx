import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { findScripts } from '../actions/api';
import CreateButton from '../components/CreateButton';

import BrowsableEntity from '../components/BrowsableEntity';

class ScriptList extends Component {
  renderEntity = (e) => {
    const { scriptId, name, archived } = e;
    return (
      <div className={`${archived ? 'archived' : ''}`}>
        <Link to={`/script/by-script-id/${scriptId}`} className="text-black">
          {name}
          <br />
          <small>{scriptId}</small>
        </Link>
      </div>
    );
  };

  render() {
    return (
      <div>
        <div className="btn-toolbar float-right">
          <CreateButton to="/script/create">Create Script</CreateButton>
        </div>
        <BrowsableEntity
          title="Browse Scripts"
          entity={this.props.scripts.find}
          find={this.props.findScripts}
          renderItem={this.renderEntity}
          allowArchived
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  scripts: state.scripts,
});

const mapDispatchToProps = {
  findScripts,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScriptList);
