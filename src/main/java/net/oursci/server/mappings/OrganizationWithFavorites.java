package net.oursci.server.mappings;

import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;
import net.oursci.server.invitation.Invitation;
import net.oursci.server.organization.favorite.OrganizationFavorite;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.organization.preferences.OrganizationPreferences;
import net.oursci.server.user.User;

@Data
public class OrganizationWithFavorites {
	private Long id;
	private String organizationId;
	private String name;
	private String description;
	private String content;
	private String icon;
	private Date date;
	private String picture;
	private List<OrganizationMember> members;
	private List<Invitation> invitations;
	private User creator;
	private OrganizationPreferences preferences;
	private boolean archived;
	private Map<String, List<OrganizationFavorite>> favorites;
	
}
