package net.oursci.server.mappings;

import lombok.Data;
import net.oursci.server.surveyresult.SurveyResult;

@Data
public class UserProfile {
	private Long id;
	private String username;
	private String displayName;
	private String picture;
	private String email;
	private String[] roles;
	private SurveyResult lastContribution;
}
