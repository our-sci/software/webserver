package net.oursci.server.mappings;

import java.util.ArrayList;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;

import net.oursci.server.surveyresult.SurveyResultService;
import net.oursci.server.user.User;

@Configuration
public class Mapper {
	public final static String MODEL_MAPPER = "ModelMapperWeb";

	@Autowired
	SurveyResultService surveyResultService;
	
	@Bean(name = MODEL_MAPPER)
	public ModelMapper modelMapper() {
		ModelMapper mapper = new ModelMapper();
		
		mapper.addConverter(new Converter<User, UserProfile>() {

			public UserProfile convert(MappingContext<User, UserProfile> context) {
				User user = context.getSource();
				UserProfile userProfile = context.getDestination();
								
				
				ArrayList<String> authorities = new ArrayList<>();
				
				for(GrantedAuthority authority : user.getAuthorities()) {
					authorities.add(authority.getAuthority());
				}
				
				userProfile.setId(user.getId());
				userProfile.setRoles(authorities.toArray(new String[authorities.size()]));
				userProfile.setEmail(user.getEmail());
				userProfile.setDisplayName(user.getDisplayName() == null ? user.getEmail() : user.getDisplayName());
				userProfile.setPicture(user.getPicture());
				userProfile.setUsername(user.getUsername());
				
				userProfile.setLastContribution(surveyResultService.getLastContribution(user));

				return userProfile;
			}
		});
		
		
		

		return mapper;
	}
}