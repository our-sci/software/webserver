package net.oursci.server.script;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.BaseException;
import net.oursci.server.exception.ScriptNotFound;
import net.oursci.server.organization.Organization;

@Service
public class ScriptServiceImpl implements ScriptService {

	@Autowired
	ScriptRepository repo;
	
	@Override
	public Script findOne(Long id) throws BaseException {
		return repo.findById(id).orElseThrow(() -> new ScriptNotFound());
	}

	@Override
	public Script save(Script script) {
		return repo.save(script);
	}

	@Override
	public Script findByIdentifier(String identifier) {
		Script script = repo.findByScriptId(identifier);
		if(script == null) {
			throw new ScriptNotFound();
		}
		return script;
	}

	@Override
	public Page<Script> find(String search, Pageable pageable) {
		return repo.find(search, false, pageable);
	}

	@Override
	public Page<Script> find(String search, Organization organization, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Script> findArchived(String search, Pageable pageable) {
		return repo.find(search, true, pageable);
	}

}
