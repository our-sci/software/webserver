package net.oursci.server.script;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ScriptRepository extends PagingAndSortingRepository<Script, Long> {
	
	Collection<Script> findAll();
	Script findByScriptId(String scriptId);
	
	Page<Script> findByNameContainingAndArchivedFalse(String search, Pageable pageable);
	Page<Script> findByNameContainingOrScriptIdIsAndArchivedFalse(String search, String scriptId, Pageable pageable);

	Page<Script> findByNameContainingAndArchivedTrue(String search, Pageable pageable);
	
	@Query("SELECT s from Script s WHERE (s.name LIKE CONCAT('%',:search,'%') OR s.scriptId = :search) AND archived = :archived")
	Page<Script> find(@Param("search") String search, @Param("archived") boolean archived, Pageable pageable);

	
}
