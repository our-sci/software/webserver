package net.oursci.server.sharecode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.surveyresult.SurveyResult;
import net.oursci.server.views.Views;

@Data
@Entity
@Table(name="sharecode")
public class Sharecode {
	
	public static final int VIEW_PUBLIC = 0;
	public static final int VIEW_ANON = 1;
	public static final int CLAIM = 2;
	

	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;
	
	@Column
	@JsonView(Views.Summary.class)
	private String code;
	
	@Column
	@JsonView(Views.Summary.class)
	private String email;
	
	@OneToOne
	@JsonView(Views.Summary.class)
	private SurveyResult surveyResult;
	
	@OneToOne
	@JsonView(Views.Summary.class)
	private Dashboard dashboard;
	
	@Column
	@JsonView(Views.Summary.class)
	private int type = VIEW_PUBLIC;
	
	@Column
	@JsonView(Views.Summary.class)
	private boolean archived = false;
	
}
