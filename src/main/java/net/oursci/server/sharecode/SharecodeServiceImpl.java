package net.oursci.server.sharecode;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.mail.MailService;
import net.oursci.server.surveyresult.SurveyResult;

@Service
public class SharecodeServiceImpl implements SharecodeService {

	@Autowired
	SharecodeRepository repo;
	
	@Autowired
	MailService mailService;

	@Value("${app.sharecode.prefix}")
	String prefix;

	@Value("${app.sharecode.suffix}")
	String suffix;

	@Override
	public Sharecode get(Long id) {
		return repo.findById(id).orElse(null);
	}

	@Override
	public void archive(Long id) {
		Sharecode sharecode = repo.findById(id).orElse(null);
		if(sharecode != null) {
			sharecode.setArchived(true);
			repo.save(sharecode);
		}
	}

	@Override
	public Sharecode add(SurveyResult sr, Dashboard db, String email) {
		return add(sr, db, email, Sharecode.VIEW_PUBLIC);
	}

	@Override
	public Sharecode add(SurveyResult sr, Dashboard dashboard, String email, int type) {
		String code = email2code(email);

		Sharecode sharecode = find(sr, dashboard, email);
		if (sharecode == null) {
			sharecode = new Sharecode();
			sharecode.setSurveyResult(sr);
			sharecode.setDashboard(dashboard);
			sharecode.setCode(code);
			sharecode.setEmail(email);
		}

		sharecode.setType(type);
		sharecode.setArchived(false);

		return repo.save(sharecode);
	}

	@Override
	public List<Sharecode> getByCode(String code) {
		return repo.findByCodeAndArchivedFalse(code);
	}

	@Override
	public List<Sharecode> getByEmail(String email) {
		return repo.findByEmailAndArchivedFalse(email);
	}
	
	@Override
	public List<Sharecode> getBySurveyResult(SurveyResult surveyResult) {
		return repo.findBySurveyResultAndArchivedFalse(surveyResult);
	}

	@Override
	public String email2code(String email) {
		return DigestUtils.sha256Hex(String.format("%s%s%s", prefix, email, suffix));
	}

	@Override
	public Sharecode find(SurveyResult sr, Dashboard dashboard, String email) {
		return repo.findOneBySurveyResultAndDashboardAndEmail(sr, dashboard, email);
	}

	@Override
	public List<String> getInstanceIdsByCode(String code) {
		List<String> list = new ArrayList<>();
		List<Sharecode> sharecodes = repo.findByCodeAndArchivedFalse(code);
		for (Sharecode sharecode : sharecodes) {
			list.add(sharecode.getSurveyResult().getInstanceId());
		}
		return list;
	}

	@Override
	public void sendLink(String email) {
		String code = email2code(email);
		mailService.sendMail(email, "Your personal Our-Sci share link", String.format("Hi there,\n\nUse the following link to access your personal shares:\nhttps://app.our-sci.net/#/share/view/%s\n\nHave a nice day!", code));
	}

	

}
