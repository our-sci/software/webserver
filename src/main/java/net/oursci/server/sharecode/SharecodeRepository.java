package net.oursci.server.sharecode;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.surveyresult.SurveyResult;

public interface SharecodeRepository extends PagingAndSortingRepository<Sharecode, Long>{

	List<Sharecode> findByCodeAndArchivedFalse(String code);
	List<Sharecode> findByEmailAndArchivedFalse(String email);
	List<Sharecode> findBySurveyResultAndArchivedFalse(SurveyResult surveyResult);
		
	//Sharecode findOneBySurveyResultAndCodeAndEmail(SurveyResult sr, String code, String email);
	Sharecode findOneBySurveyResultAndDashboardAndEmail(SurveyResult sr, Dashboard dashboard, String email);
	
	
}
