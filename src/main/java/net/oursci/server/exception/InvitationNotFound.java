package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class InvitationNotFound extends BaseException {
	private static final long serialVersionUID = -4673440062090285489L;

	public InvitationNotFound(String message) {
		super(message);
	}

	public InvitationNotFound() {
		super("Invitation not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}

