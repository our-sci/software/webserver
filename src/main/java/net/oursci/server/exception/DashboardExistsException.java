package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class DashboardExistsException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2435970492564178231L;

	public DashboardExistsException(String message) {
		super(message);
	}

	public DashboardExistsException() {
		super("This dashboard already exists");
	}
	
	@Override
	public int getErrorCode() {
		
		return HttpStatus.CONFLICT.value();
	}
}

