package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class OrganizationAlreadyExists extends BaseException {
	
	private static final long serialVersionUID = 8373112792083779307L;

	public OrganizationAlreadyExists(String message) {
		super(message);
	}

	public OrganizationAlreadyExists() {
		super("An organization with the same ID already exists");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.CONFLICT.value();
	}
}
