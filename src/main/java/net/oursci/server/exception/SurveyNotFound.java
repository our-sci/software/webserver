package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class SurveyNotFound extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3406495648306502007L;

	public SurveyNotFound(String message) {
		super(message);
	}

	public SurveyNotFound() {
		super("Survey not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}

