package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class NotOrganizationAdmin extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5024964314490822926L;

	public NotOrganizationAdmin(String message) {
		super(message);
	}

	public NotOrganizationAdmin() {
		super("You are not admin of this organization");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.FORBIDDEN.value();
	}
}
