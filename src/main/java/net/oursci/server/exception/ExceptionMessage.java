package net.oursci.server.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExceptionMessage {
	public Integer status;
	public String key;
	public String description;
	
	public ExceptionMessage(){
		
	}
	
	public ExceptionMessage(Integer status, String key, String description) {
		this.status = status;
		this.key = key;
		this.description = description;
	}
	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
		
	}
}
