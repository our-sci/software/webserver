package net.oursci.server.organization.favorite;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import net.oursci.server.organization.Organization;

public interface OrganizationFavoriteRepository extends CrudRepository<OrganizationFavorite, Long> {
	
	List<OrganizationFavorite> findAll();
	
	List<OrganizationFavorite> findByOwner(Organization owner);
	List<OrganizationFavorite> findByOwnerAndType(Organization owner, String type);
	List<OrganizationFavorite> findByOwnerAndTypeOrderByPriorityDesc(Organization owner, String type);

	
}
