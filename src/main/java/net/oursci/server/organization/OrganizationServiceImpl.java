package net.oursci.server.organization;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.OrganizationNotFound;
import net.oursci.server.user.User;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	OrganizationRepository repo;
	
	@Override
	public Organization save(Organization organization) {
		return repo.save(organization);
	}
	
	@Override
	public Organization findOne(Long id) {
		return repo.findById(id).orElseThrow(() -> new OrganizationNotFound());
	}

	@Override
	public Organization findByOrganizationId(String organizationId) {
		Organization organization = repo.findOneByOrganizationId(organizationId);
		return organization;
	}

	@Override
	public List<Organization> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Organization> findByUser(User user) {
		return repo.findByUser(user);
	}

	@Override
	public boolean exists(Long id) {
		return repo.existsById(id);
	}

	@Override
	public boolean exists(String organizationId) {
		Organization organization = repo.findOneByOrganizationId(organizationId);
		if(organization != null) {
			return true;
		}
		return false;
	}

	@Override
	public Organization getByOrganizationId(String organizationId) {
		Organization organization = repo.findOneByOrganizationId(organizationId);
		if(organization == null){
			throw new OrganizationNotFound();
		}
		return organization;
	}
	
	@Override
	public Page<Organization> find(String search, Pageable pageable) {
		return repo.find(search, false, pageable);
	}

	@Override
	public Page<Organization> findArchived(String search, Pageable pageable) {
		return repo.find(search, true, pageable);
	}



}
