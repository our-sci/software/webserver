package net.oursci.server.organization.member;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

@Data
@Entity
@Table(name = "organizationmember")
public class OrganizationMember {

	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="organization_id")
	private Organization organization;
	
	@OneToOne
	private User user;
	
	@Column(name = "role")
	private Long role;
	
	@Column(name = "admin")
	private boolean admin = false;
	
	@Column(name = "description")
	private String description;
	
}
