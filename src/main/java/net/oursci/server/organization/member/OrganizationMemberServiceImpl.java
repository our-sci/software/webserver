package net.oursci.server.organization.member;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.MemberAlreadyExists;
import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

@Service
public class OrganizationMemberServiceImpl implements OrganizationMemberService {

	@Autowired
	OrganizationMemberRepository repo;
	
	@Override
	public List<OrganizationMember> findByUser(User user) {
		return repo.findByUser(user);
	}

	@Override
	public OrganizationMember find(Long id) {
		return repo.findById(id).orElse(null);
	}

	@Override
	public OrganizationMember save(OrganizationMember member) {
		return repo.save(member);
	}

	@Override
	public void delete(OrganizationMember member) {
		repo.delete(member);
	}

	@Override
	public OrganizationMember findOneByUserAndOrganization(User user, Organization organization) {
		return repo.findOneByUserAndOrganization(user, organization);
	}

	@Override
	public OrganizationMember add(OrganizationMember member) {
		OrganizationMember existing = repo.findOneByUserAndOrganization(member.getUser(), member.getOrganization());
		if(existing != null) {
			throw new MemberAlreadyExists("User already joined organization "+member.getOrganization().getName());
		}
		
		return repo.save(member);
	}

}
