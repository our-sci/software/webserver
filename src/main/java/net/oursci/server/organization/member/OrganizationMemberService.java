package net.oursci.server.organization.member;

import java.util.List;

import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

public interface OrganizationMemberService {

	OrganizationMember add(OrganizationMember member);
	
	OrganizationMember find(Long id);
	List<OrganizationMember> findByUser(User user);
	OrganizationMember save(OrganizationMember member);
	void delete(OrganizationMember member);
	
	OrganizationMember findOneByUserAndOrganization(User currentUser, Organization organization);
}
