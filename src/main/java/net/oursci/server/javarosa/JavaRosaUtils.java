package net.oursci.server.javarosa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.wink.json4j.OrderedJSONObject;
import org.javarosa.core.model.FormDef;
import org.javarosa.core.model.FormIndex;
import org.javarosa.core.model.instance.FormInstance;
import org.javarosa.core.model.instance.TreeElement;
import org.javarosa.form.api.FormEntryController;
import org.javarosa.form.api.FormEntryModel;
import org.javarosa.xform.parse.XFormParseException;
import org.javarosa.xform.parse.XFormParser;
import org.javarosa.xform.util.XFormUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.oursci.server.config.OursciConfig;
import net.oursci.server.util.ApplicationInfo;

@Service
public class JavaRosaUtils {

	public static String appUrlBase;
	
	@Value("${app.url.base}")
    public void setBaseUrl(String baseUrl) {
        appUrlBase = baseUrl;
    }
	
	public static String getFormId(FormInstance formInstance) {
		TreeElement root = formInstance.getRoot();
		for (int i = 0; i < root.getAttributeCount(); i++) {
			String attrName = root.getAttributeName(i);
			String attrValue = root.getAttributeValue(i);
			if (attrName.equalsIgnoreCase("id")) {
				return attrValue;
			}
		}
		return null;
	}

	public static String getFormId(FormDef formDef) {
		return getFormId(formDef.getMainInstance());
	}

	public static String getMeta(FormInstance formInstance, String key) {
		TreeElement root = formInstance.getRoot();
		TreeElement meta = root.getChild("meta", 0);
		TreeElement element = meta.getChild(key, 0);
		String ret;
		try {
			ret = (String) element.getValue().getValue();
		} catch (Exception e) {
			ret = "";
		}
		return ret;
	}

	public static String getStringValue(FormInstance formInstance, String key) {
		TreeElement root = formInstance.getRoot();

		for (int i = 0; i < root.getNumChildren(); i++) {
			TreeElement child = root.getChildAt(i);
			String keyname = child.getName();
			if (keyname.equalsIgnoreCase(key)) {
				return getStringValue(child);
			}

			if (child.hasChildren()) {
				for (int j = 0; j < child.getNumChildren(); j++) {
					TreeElement grandchild = child.getChildAt(j);
					String grandkeyname = grandchild.getName();
					if (grandkeyname.equalsIgnoreCase(key)) {
						return getStringValue(grandchild);
					}
				}
			}
		}

		return null;
	}
	
	private static String getStringValue(TreeElement element) {
		String value = "";
		try {
			value = (String) element.getValue().getValue();
			return value;
		} catch (Exception e) {
			return null;
		}
	}

	public static String getInstanceID(FormInstance formInstance) {
		return getMeta(formInstance, "instanceID");
	}

	public static String getUserID(FormInstance formInstance) {
		return getMeta(formInstance, "userID");
	}

	public static String getModifiedTimestamp(FormInstance formInstance) {
		return getMeta(formInstance, "modified");
	}

	public static String getCreatedTimestamp(FormInstance formInstance) {
		return getMeta(formInstance, "created");
	}

	public static OrderedJSONObject getJsonKeysAndValues(FormDef formDef, FormInstance formInstance, boolean revealAnon,
			String groupDelimiter) {
		OrderedJSONObject json = new OrderedJSONObject();
		TreeElement root = formInstance.getRoot();
		// String formId = getFormId(formInstance);
		String instanceID = getInstanceID(formInstance);
		String created = getCreatedTimestamp(formInstance);
		String modified = getModifiedTimestamp(formInstance);
		String userID = getUserID(formInstance);

		ArrayList<String> anon = new ArrayList<>();

		for (int i = 0; i < root.getNumChildren(); i++) {
			if (revealAnon) {
				break;
			}

			TreeElement meta = root.getChildAt(i);
			if (!meta.getName().equalsIgnoreCase("meta")) {
				continue;
			}

			for (int j = 0; j < meta.getNumChildren(); j++) {
				TreeElement child = meta.getChildAt(j);
				if (!meta.getChildAt(j).getName().equalsIgnoreCase("anonymize")) {
					continue;
				}

				for (int k = 0; k < child.getNumChildren(); k++) {
					TreeElement id = child.getChildAt(k);
					if (!id.getName().equalsIgnoreCase("id")) {
						break;
					}

					try {
						String anonId = extractName((String) id.getValue().getValue(), groupDelimiter);
						anon.add(anonId);
					} catch (Exception e) {
					}
				}
			}
		}

		for (int i = 0; i < root.getNumChildren(); i++) {
			TreeElement child = root.getChildAt(i);

			String key = child.getName();
			if (key.equalsIgnoreCase("meta")) {
				continue;
			}

			if (child.hasChildren()) {
				for (int j = 0; j < child.getNumChildren(); j++) {
					TreeElement groupChild = child.getChildAt(j);

					final String identifier = String.format("%s%s%s", child.getName(), groupDelimiter,
							groupChild.getName());

					if (anon.contains(identifier)) {
						try {
							json.put(identifier, "");
						} catch (org.apache.wink.json4j.JSONException e1) {
							e1.printStackTrace();
						}
						continue;
					}
					processChild(groupChild, identifier, json, formInstance);
				}
				continue;
			}

			final String identifier = child.getName();

			if (anon.contains(identifier)) {
				try {
					json.put(identifier, "");
				} catch (org.apache.wink.json4j.JSONException e1) {
					e1.printStackTrace();
				}
				continue;
			}
			processChild(child, identifier, json, formInstance);

		}

		try {
			json.put("metaDateCreated", created);
			json.put("metaDateModified", modified);
			json.put("metaInstanceID", instanceID);
			json.put("metaUserID", userID);
		} catch (org.apache.wink.json4j.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return json;
	}

	private static void processChild(TreeElement child, String identifier, OrderedJSONObject json,
			FormInstance formInstance) {
		String value = "";
		try {
			value = (String) child.getValue().getValue();
		} catch (Exception e) {
			// just go on
		}

		try {
			json.put(identifier, value);
		} catch (org.apache.wink.json4j.JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*
		 * if(value.startsWith("{")){ LinkedHashMap<String, String> orderedMap = new
		 * LinkedHashMap<>();
		 * 
		 * String[] lines = value.split("\n"); for(String line : lines) { Matcher m =
		 * PATTERN.matcher(line); if(m.find()){ String subkey = m.group(1); String
		 * subvalue = m.group(2);
		 * //System.out.println("Subkey="+subkey+", Subvalue="+subvalue);
		 * orderedMap.put(subkey, subvalue); } } JSONObject orderedJson = new
		 * JSONObject(orderedMap); try { json.put(key, orderedJson); } catch
		 * (JSONException e) { // just go on }
		 * System.out.println(orderedJson.toString()); JSONArray jsonArray = new
		 * JSONArray(Arrays.asList(orderedJson)); try {
		 * System.out.println("Ordered JSON Final CSV :: "+CDL.toString( jsonArray)); }
		 * catch (JSONException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */

		try {
			OrderedJSONObject object = new OrderedJSONObject(value);

			OrderedJSONObject meta = (OrderedJSONObject) object.get("meta");

			String filename = meta.getString("filename");
			if (filename != null) {
				//String region = ApplicationInfo.REGION;
				//String bucket = ApplicationInfo.BUCKET;
				String encInstanceID = URLEncoder.encode(getInstanceID(formInstance), OursciConfig.CHARSET);
				String encFormId = URLEncoder.encode(getFormId(formInstance), OursciConfig.CHARSET);
				String encFilename = URLEncoder.encode(filename, OursciConfig.CHARSET);

				//String url = String.format("https://s3.%s.amazonaws.com/%s/survey-results/%s/%s/%s", region, bucket,
				//		encFormId, encInstanceID, encFilename);
				String url = String.format("%s/storage/survey-results/%s/%s/%s", ApplicationInfo.URL_BASE, encFormId, encInstanceID, encFilename);
				meta.put("filename", url);
			}

			json.put(identifier, object);
		} catch (Exception e) {
			// just go on
			// e.printStackTrace();
		}

	}

	public static OrderedJSONObject getJsonKeysAndValues(FormDef formDef, InputStream formInstanceStream,
			boolean revealAnon, String groupDelimiter) throws IOException {
		FormInstance formInstance = getFormInstance(formInstanceStream);
		formInstanceStream.close();

		return getJsonKeysAndValues(formDef, formInstance, revealAnon, groupDelimiter);
	}

	public static String getJsonKeys(FormDef formDef) {
		JSONArray array = new JSONArray();

		TreeElement root = formDef.getMainInstance().getRoot();
		array.put("metaDate");
		array.put("metaInstanceID");
		array.put("metaUserID");

		for (int i = 0; i < root.getNumChildren(); i++) {
			TreeElement child = root.getChildAt(i);
			String key = child.getName();
			if (key.equalsIgnoreCase("meta"))
				continue;

			array.put(key);
		}

		return array.toString();
	}

	public static List<String> getKeys(FormDef formDef, String groupDelimiter) {
		ArrayList<String> array = new ArrayList<>();
		// TreeElement root = formDef.getMainInstance().getRoot();
		array.add("metaDateCreated");
		array.add("metaDateModified");
		array.add("metaInstanceID");
		array.add("metaUserID");

		final FormEntryController fec = new FormEntryController(new FormEntryModel(formDef));
		FormIndex formIndex = fec.getModel().getFormIndex();

		int event = -1;
		while ((event = fec.getModel().getEvent(formIndex)) != FormEntryController.EVENT_END_OF_FORM) {
			switch (event) {
			case FormEntryController.EVENT_QUESTION:
				array.add(extractName(fec.getModel().getQuestionPrompt().getQuestion().getTextID(), groupDelimiter));
				break;
			default:
				break;
			}

			formIndex = fec.getModel().incrementIndex(formIndex);
			fec.getModel().setQuestionIndex(formIndex);
		}
		return array;
	}

	private static String extractName(String textID, String groupDelimiter) {
		try {
			int end = textID.lastIndexOf(":");
			// String[] parts = textID.substring("/data/".length(), end).split("/");
			String ret = textID.substring("/data/".length(), end).replaceAll("\\/", groupDelimiter);
			// String ret = String.format("%s%s%s", parts[parts.length - 2], groupDelimiter,
			// parts[parts.length - 1]);

			return ret;
		} catch (Exception e) {
			e.printStackTrace();
			return textID;
		}
	}

	public static String getJsonKeys(InputStream formDefStream) {
		FormDef formDef = getFormDef(formDefStream);
		return getJsonKeys(formDef);
	}

	public static String getJsonKeys(File file) throws FileNotFoundException {
		return getJsonKeys(new FileInputStream(file));
	}

	public static List<String> getKeys(InputStream formDefStream, String groupDelimiter) {
		FormDef formDef = getFormDef(formDefStream);
		return getKeys(formDef, groupDelimiter);
	}

	public static List<String> getKeys(File file, String groupDelimiter) throws FileNotFoundException {
		return getKeys(new FileInputStream(file), groupDelimiter);
	}

	public static FormInstance getFormInstance(InputStream formInstanceStream) throws IOException {
		return XFormParser.restoreDataModel(formInstanceStream, null);
	}

	public static FormDef getFormDef(InputStream formDefStream) throws XFormParseException {
		return XFormUtils.getFormFromInputStream(formDefStream);
	}
}
