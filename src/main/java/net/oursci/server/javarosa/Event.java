package net.oursci.server.javarosa;


import org.javarosa.form.api.FormEntryController;

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 30.08.17.
 */

public enum Event {

    EVENT_BEGINNING_OF_FORM(FormEntryController.EVENT_BEGINNING_OF_FORM),
    EVENT_END_OF_FORM(FormEntryController.EVENT_END_OF_FORM),
    EVENT_PROMPT_NEW_REPEAT(FormEntryController.EVENT_PROMPT_NEW_REPEAT),
    EVENT_QUESTION(FormEntryController.EVENT_QUESTION),
    EVENT_GROUP(FormEntryController.EVENT_GROUP),
    EVENT_REPEAT(FormEntryController.EVENT_REPEAT),
    EVENT_REPEAT_JUNCTURE(FormEntryController.EVENT_REPEAT_JUNCTURE),
    EVENT_UNKNOWN(-1);

    private final int mState;

    Event(int state) {
        mState = state;
    }

    public static Event fromEvent(int event) {
        for (Event e : Event.values()) {
            if (e.mState == event) {
                return e;
            }
        }

        return EVENT_UNKNOWN;
    }

}

