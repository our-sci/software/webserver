package net.oursci.server.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.BaseException;
import net.oursci.server.exception.DashboardNotFound;
import net.oursci.server.organization.Organization;

@Service
public class DashboardServiceImpl implements DashboardService{

	@Autowired
	DashboardRepository repo;
	
	@Override
	public Dashboard findOne(Long id) throws BaseException {
		return repo.findById(id).orElseThrow(() -> new DashboardNotFound());
	}

	@Override
	public Dashboard save(Dashboard dashboard) {
		return repo.save(dashboard);
	}

	@Override
	public Dashboard findByIdentifier(String identifier) {
		Dashboard dashboard = repo.findByDashboardId(identifier);
		if(dashboard == null) {
			throw new DashboardNotFound(identifier);
		}
		return dashboard;
	}

	@Override
	public Page<Dashboard> find(String search, Pageable pageable) {
		return repo.find(search, false, pageable);
	}

	@Override
	public Page<Dashboard> find(String search, Organization organization, Pageable pageable) {
		return repo.findByOrganization(search, organization, pageable);
	}

	@Override
	public Page<Dashboard> findArchived(String search, Pageable pageable) {
		return repo.find(search, true, pageable);

	}

}
