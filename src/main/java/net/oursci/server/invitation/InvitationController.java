package net.oursci.server.invitation;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.exception.BadRequest;
import net.oursci.server.exception.NotOrganizationAdmin;
import net.oursci.server.mail.MailService;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.organization.member.OrganizationMemberService;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.util.OursciUtils;

@RestController
@RequestMapping(value = "/api/invitation")
public class InvitationController {

	@Autowired
	InvitationService service;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	OrganizationMemberService organizationMemberService;

	@Autowired
	OursciUtils utils;

	@Autowired
	UserService userService;
	
	@Autowired
	MailService mailService;
	
	
	@RequestMapping("/create")
	public List<Invitation> create(@RequestParam String organizationId, @RequestParam(required = false) String emails) {
		Organization organization = organizationService.getByOrganizationId(organizationId);

		try {
			utils.assertOrganizationAdmin(organization);
		} catch (Exception e) {
			throw new NotOrganizationAdmin("Only organization admins may create invitations.");
		}

		ArrayList<Invitation> ret = new ArrayList<>();

		if (emails == null || emails.trim().isEmpty()) {
			Invitation inv = createInvitation(organization, null);
			ret.add(inv);
			return ret;
		}

		String[] splits = emails.split(";");
		for (int i = 0; i < splits.length; i++) {
			String email = splits[i].trim();
			Invitation inv = createInvitation(organization, email);
			ret.add(inv);
		}

		return ret;

	}

	@Value("${app.invitation.email.subject}")
	String subject;
	
	@Value("${app.invitation.email.body}")
	String body;
	
	private Invitation createInvitation(Organization organization, String email) {
		final DateTime now = new DateTime();

		final String token = UUID.randomUUID().toString();
		final Date issued = now.toDate();
		final Date expires = now.plusDays(14).toDate();

		Invitation i = new Invitation();
		i.setOrganization(organization);
		i.setCode(token);
		i.setIssued(issued);
		i.setExpires(expires);
		if (email != null && !email.isEmpty()) {
			i.setEmail(email);
		}

		Invitation saved = service.save(i);

		if (i.getEmail() != null) {
			System.out.println("Sending emails, writing emails...");
			String s = MessageFormat.format(subject, organization.getName());
			String b = MessageFormat.format(body, organization.getName(), i.getCode());
			mailService.sendMail(i.getEmail(), s, b);
		}

		return saved;
	}

	@RequestMapping("/delete")
	private void deleteInvitation(@RequestParam Long id) {
		Invitation i = service.getById(id);
		utils.assertOrganizationAdmin(i.getOrganization());

		service.delete(i);
	}

	@RequestMapping("/self")
	public List<Invitation> getMyInvitations() {
		User user = utils.getCurrentUser();
		return service.findByUser(user);
	}

	@RequestMapping("/user")
	public List<Invitation> getInvitiationsByUser(@RequestParam Long id) {
		User user = userService.getById(id);
		return service.findByUser(user);
	}

	@RequestMapping("/claim")
	public Invitation claim(@RequestParam String code) {
		User user = utils.getCurrentUser();
		Invitation inv = claimInvitationCode(user, code);
		return inv;
	}

	@RequestMapping("/showcode")
	public String showCode(@RequestParam Long id) {
		Invitation invitation = service.getById(id);
		utils.assertOrganizationAdmin(invitation.getOrganization());
		return invitation.getCode();
	}

	@RequestMapping("/test")
	public Invitation test(@RequestParam String code) {
		return service.findByCode(code);
	}
	
	@RequestMapping("/resend")
	public boolean resend(@RequestParam Long id, String subject, String body) {
		
		Invitation i = service.getById(id);
		utils.assertOrganizationAdmin(i.getOrganization());
		
		return mailService.sendMail(i.getEmail(), subject, body);
	}

	private Invitation claimInvitationCode(User user, String code) {

		if (code == null || code.isEmpty()) {
			throw new BadRequest("No inviation found.");
		}

		Invitation invitation = service.findByCode(code.trim());
		if (invitation == null) {
			throw new BadRequest("No invitation found for code: " + code);
		}

		if (invitation.getClaimed() != null) {
			throw new BadRequest("Code was already claimed: " + code);
		}

		Organization organization = invitation.getOrganization();
		boolean admin = invitation.isAdmin();

		OrganizationMember om = new OrganizationMember();
		om.setOrganization(organization);
		om.setAdmin(admin);
		om.setUser(user);
		om.setDescription("");
		om.setRole(0L);

		om = organizationMemberService.add(om);

		invitation.setMember(om);
		invitation.setClaimed(new Date());
		invitation.setStatus(InvitationStatus.ACCEPTED);

		return service.save(invitation);

	}

}
