package net.oursci.server.invitation;

public enum InvitationStatus {
	IDLE,
	DECLINED,
	ACCEPTED
}
