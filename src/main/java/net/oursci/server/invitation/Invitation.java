package net.oursci.server.invitation;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.user.User;
import net.oursci.server.views.Views;

@Data
@Entity
@Table(name = "invitation")
public class Invitation {

	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;

	@ManyToOne
	@JoinColumn(name="organization_id")
	@JsonManagedReference
	private Organization organization;
	
	private Date issued;
	
	private Date expires;
	
	private Date claimed;
	
	private String email;
	
	@OneToOne
	private OrganizationMember member;
	
	@OneToOne
	private User user;
	
	private boolean admin = false;
	
	@JsonIgnore
	@Column(unique=true)
	private String code;
	
	@Enumerated(EnumType.STRING)
	private InvitationStatus status = InvitationStatus.IDLE;
	
}
