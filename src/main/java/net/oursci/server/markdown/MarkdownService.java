package net.oursci.server.markdown;

public interface MarkdownService {
	public String render(String markdown);
}
