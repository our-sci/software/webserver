package net.oursci.server.tpcredential;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import net.oursci.server.user.User;

public interface TpCredentialRepository extends PagingAndSortingRepository<TpCredential, Long> {

	List<TpCredential> findAll();
	List<TpCredential> findByUser(User user);
	List<TpCredential> findByUserAndType(User user, TpCredentialType type);
	
	@Query("SELECT tpc FROM TpCredential tpc WHERE tpc.url LIKE CONCAT('%',:search,'%') OR tpc.username LIKE CONCAT('%',:search,'%')")
	Page<TpCredential> find(@Param("search") String search, Pageable pageable);
	
	@Query("SELECT tpc FROM TpCredential tpc WHERE tpc.user = :user AND ( tpc.url LIKE CONCAT('%',:search,'%') OR tpc.username LIKE CONCAT('%',:search,'%') )")
	Page<TpCredential> find(@Param("search") String search, @Param("user") User user, Pageable pageable);
	
}
