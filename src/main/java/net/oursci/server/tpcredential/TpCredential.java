package net.oursci.server.tpcredential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import net.oursci.server.user.User;

@Data
@Entity
@Table(name = "tpcredential")
public class TpCredential {

	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	private String url;
	
	private String username;
	
	private String password;
	
	@Lob
	private String token;
	
	@Lob
	private String parameters;
	
	@Lob
	private String note;
	
	@Enumerated(EnumType.STRING)
	private TpCredentialType type = TpCredentialType.DEFAULT;
	
}
