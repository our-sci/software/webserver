package net.oursci.server.tpcredential;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.oursci.server.user.User;

public interface TpCredentialService {

	TpCredential save(TpCredential a);
	void delete(Long id);
	TpCredential getById(Long id);
	boolean exists(Long id);
	
	List<TpCredential> list();
	List<TpCredential> list(User user);
	
	List<TpCredential> findByUser(User user);
	List<TpCredential> findByUserAndType(User user, TpCredentialType type);
	
	Page<TpCredential> find(String search, Pageable pageable);
	Page<TpCredential> find(String search, User user, Pageable pageable);
	
}
