package net.oursci.server.user;

import org.springframework.data.repository.CrudRepository;


public interface RoleRepository extends CrudRepository<Role, Long> {

	Role findByAuthority(String authority);
}