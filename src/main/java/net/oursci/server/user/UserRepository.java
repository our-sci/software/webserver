package net.oursci.server.user;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	List<User> findAll();
	
	User findOneByUsername(String username);
	User findOneByEmail(String email);
	
	@Query("SELECT user FROM User user WHERE user.email LIKE CONCAT('%',:search,'%') OR user.displayName LIKE CONCAT('%',:search,'%') OR user.username = :search")
	Page<User> find(@Param("search") String search, Pageable pageable);

}
