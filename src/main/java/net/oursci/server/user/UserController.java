package net.oursci.server.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.config.SecurityConfig.Roles;

@RestController
@RequestMapping("/api/user")
public class UserController {

	
	@Autowired
	UserService service;
	
	@Secured(value = {Roles.ROLE_ADMIN})
	@RequestMapping("/find")
	public Page<User> find(@RequestParam(required = false, defaultValue = "") String search, Pageable pageable) {
		return service.find(search, pageable);
	}
	
}
