package net.oursci.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OursciServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OursciServerApplication.class, args);
	}
}
