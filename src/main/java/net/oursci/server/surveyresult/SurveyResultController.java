package net.oursci.server.surveyresult;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.javarosa.core.model.FormDef;
import org.javarosa.core.model.instance.FormInstance;
import org.javarosa.xform.parse.XFormParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.dashboard.DashboardService;
import net.oursci.server.exception.DashboardNotFound;
import net.oursci.server.exception.SurveyResultExistsException;
import net.oursci.server.exception.XmlFormNotFound;
import net.oursci.server.javarosa.JavaRosaUtils;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationRepository;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.sharecode.Sharecode;
import net.oursci.server.sharecode.SharecodeService;
import net.oursci.server.survey.Survey;
import net.oursci.server.survey.SurveyRepository;
import net.oursci.server.survey.SurveyService;
import net.oursci.server.user.Contribution;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.util.OursciUtils;
import net.oursci.server.websocket.Statistics;

@RestController
@RequestMapping(value = "/api/survey/result")
@CrossOrigin
public class SurveyResultController {

	@Autowired
	S3Wrapper s3Wrapper;

	@Autowired
	SurveyService surveyService;

	@Autowired
	SurveyResultService surveyResultService;

	@Autowired
	SurveyRepository surveyRepository;

	@Autowired
	SurveyResultRepository surveyResultRepository;

	@Autowired
	UserService userService;

	@Autowired
	SharecodeService sharecodeService;

	@Autowired
	DashboardService dashboardService;

	@Autowired
	OursciUtils oursciUtils;

	@Autowired
	OrganizationRepository organizationRepository;

	@Autowired
	SimpMessagingTemplate webSocket;

	@Value("${app.settings.concurrent-downloads}")
	boolean concurrentDownloads;

	@RequestMapping(value = "/list/by-form-id/{formId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<SurveyResult> getSurveyResultsByFormId(@PathVariable String formId) {
		return (List<SurveyResult>) surveyResultRepository.findByFormId(formId);
	}

	public static String DELIMITER = ",";
	public static String QUOTE = "\"";
	public static String NEWLINE = "\r\n";

	@ResponseBody
	@RequestMapping(value = { "/csv/by-form-id/{formId}", "/csv/{formId}" }, method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<ByteArrayResource> getCSV(@PathVariable String formId,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date from,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date to,
			@RequestParam(name = "user", required = false) String username,
			@RequestParam(required = false) String sharecode,
			@RequestParam(required = false, defaultValue = ".") String groupDelimiter,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false, defaultValue = "false") Boolean archived) throws IOException {
		// https://blog.tratif.com/2017/11/23/effective-restful-search-api-in-spring/

		Survey survey = surveyService.getByIdentifier(formId);

		User user = null;
		if (username != null) {
			user = userService.getByUsername(username);
		}

		List<String> instanceIds = new ArrayList<>();
		if (sharecode != null) {
			instanceIds = sharecodeService.getInstanceIdsByCode(sharecode);
		}

		if (to != null) {
			to = DateUtils.addDays(to, 1);
		}

		Specification<SurveyResult> specs = Specification.where(SurveyResultSpec.withSurvey(survey))
				.and(SurveyResultSpec.withDateBetween(from, to)).and(SurveyResultSpec.withUser(user))
				.and(SurveyResultSpec.withInstanceIds(instanceIds)).and(SurveyResultSpec.withArchived(archived));
		List<SurveyResult> results = surveyResultService.findAll(specs, new Sort(Direction.DESC, "modified"));

		// optional limit (better approach would be to use Pageable object)
		if (size != null && size < results.size()) {
			results.subList(size, results.size()).clear();
		}

		// initial keys from survey definition
		List<String> initialKeys = getKeys(survey, groupDelimiter);

		// json keys and values
		JSONArray jsonResults = getJsonKeysAndValues(survey, results, groupDelimiter);

		// csv
		String csv = convertToCSV(initialKeys, jsonResults);

		ByteArrayResource resource = new ByteArrayResource(csv.getBytes());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("text/csv")).body(resource);
	}

	/*
	 * @ResponseBody
	 * 
	 * @RequestMapping(value = { "/csv/by-form-id/{formId}", "/csv/{formId}" },
	 * method = RequestMethod.GET)
	 * 
	 * @ResponseStatus(code = HttpStatus.OK) public
	 * ResponseEntity<ByteArrayResource> getCSV(HttpServletResponse
	 * response, @PathVariable String formId,
	 * 
	 * @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date
	 * from,
	 * 
	 * @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date
	 * to,
	 * 
	 * @RequestParam(required = false, defaultValue = ".") String groupDelimiter,
	 * 
	 * @RequestParam(required = false) String selection, @RequestParam(name="user",
	 * required = false) String username, @RequestParam(required = false) Integer
	 * size) throws IOException {
	 * 
	 * // TODO: use effective restful search api, e.g. //
	 * https://blog.tratif.com/2017/11/23/effective-restful-search-api-in-spring/
	 * 
	 * Survey survey = surveyService.getByIdentifier(formId); User user = null;
	 * if(username != null) { user = userService.getByUsername(username); }
	 * 
	 * // keys from survey definition List<String> initialKeys = getKeys(survey,
	 * groupDelimiter);
	 * 
	 * // result as json array (may hold more keys than initial keys)
	 * List<SurveyResult> results = getResults(survey, user, from, to, size);
	 * JSONArray jsonResults = getJsonKeysAndValues(survey, results,
	 * groupDelimiter);
	 * 
	 * // csv String csv = convertToCSV(initialKeys, jsonResults);
	 * 
	 * ByteArrayResource resource = new ByteArrayResource(csv.getBytes()); return
	 * ResponseEntity.ok().contentType(MediaType.parseMediaType("text/csv")).body(
	 * resource); }
	 */

	@SuppressWarnings("unchecked")
	private String convertToCSV(List<String> keys, JSONArray results) {
		List<String> initialKeys = new ArrayList<String>(keys);

		// fill in the keys
		for (int i = 0; i < results.length(); i++) {
			try {
				OrderedJSONObject obj = (OrderedJSONObject) results.getJSONObject(i);

				Iterator<String> topIterator = obj.getOrder();
				while (topIterator.hasNext()) {
					// keys from survey definition
					String topKey = topIterator.next();
					Object top = obj.get(topKey);
					if (top instanceof OrderedJSONObject) {
						// 'meta' or 'data'
						Iterator<String> midIterator = ((OrderedJSONObject) top).getOrder();
						while (midIterator.hasNext()) {
							String midKey = midIterator.next();
							Object mid = ((OrderedJSONObject) top).get(midKey);
							// unknown user defined keys
							Iterator<String> subIterator = ((OrderedJSONObject) mid).getOrder();
							while (subIterator.hasNext()) {
								String subKey = subIterator.next();
								// Object sub = ((OrderedJSONObject) mid).get(subKey);
								String key = topKey + "." + midKey + "." + subKey;
								if (!keys.contains(key)) {
									// System.out.println(String.format("topKey=%s, key=%s", topKey, key));
									int index = keys.indexOf(topKey);
									if (index > 0) {
										keys.add(index, key);
									}
								}
							}
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		StringBuilder stringBuilder = new StringBuilder();

		// fill in the header row
		for (String key : keys) {
			stringBuilder.append(key);
			stringBuilder.append(DELIMITER);
		}

		// remove last comma
		stringBuilder.delete(stringBuilder.lastIndexOf(DELIMITER),
				stringBuilder.lastIndexOf(DELIMITER) + DELIMITER.length());

		stringBuilder.append(NEWLINE);

		// fill in the values
		for (int i = 0; i < results.length(); i++) {
			try {
				OrderedJSONObject obj = (OrderedJSONObject) results.getJSONObject(i);

				for (int j = 0; j < keys.size(); j++) {
					String key = keys.get(j);

					if (initialKeys.contains(key)) {
						if (!obj.has(key)) { // new question has been added to
												// the survey
							stringBuilder.append("");
							stringBuilder.append(DELIMITER);
							continue;
						}
						Object o = obj.get(key);
						if (o instanceof OrderedJSONObject) {
							stringBuilder.append("");
							stringBuilder.append(DELIMITER);
						} else {
							final String part = StringEscapeUtils.escapeCsv(obj.get(key).toString());
							stringBuilder.append(part);
							stringBuilder.append(DELIMITER);
						}
						continue;
					}

					String[] splits = key.split("\\.");

					String topKey = key.substring(0, key.lastIndexOf("."));
					// keys from survey definition
					topKey = topKey.substring(0, topKey.lastIndexOf("."));
					// 'meta' or 'data'
					String midKey = splits[splits.length - 2];
					// unknown user defined keys
					String subKey = splits[splits.length - 1];

					try {
						OrderedJSONObject top = (OrderedJSONObject) obj.get(topKey);
						OrderedJSONObject mid = (OrderedJSONObject) top.get(midKey);
						String sub = mid.getString(subKey);

						stringBuilder.append(StringEscapeUtils.escapeCsv(sub));
						stringBuilder.append(DELIMITER);
					} catch (Exception e) {
						stringBuilder.append(QUOTE);
						stringBuilder.append("");
						stringBuilder.append(QUOTE);
						stringBuilder.append(DELIMITER);
					}
				}
				// remove last comma
				stringBuilder.delete(stringBuilder.lastIndexOf(DELIMITER),
						stringBuilder.lastIndexOf(DELIMITER) + DELIMITER.length());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			stringBuilder.append(NEWLINE);
		}
		stringBuilder.delete(stringBuilder.lastIndexOf(NEWLINE), stringBuilder.lastIndexOf(NEWLINE) + NEWLINE.length());

		return stringBuilder.toString();
	}

	@ResponseBody
	@RequestMapping(value = { "/json/by-form-id/{formId}",
			"/json/{formId}" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(code = HttpStatus.OK)
	public String getKeysAndValues(@PathVariable String formId,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date from,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date to,
			@RequestParam(name = "user", required = false) String username,
			@RequestParam(required = false) String sharecode,
			@RequestParam(required = false, defaultValue = ".") String groupDelimiter,
			@RequestParam(required = false, defaultValue = "false") Boolean archived) throws IOException {
		long t0 = System.currentTimeMillis();
		Survey survey = surveyService.getByIdentifier(formId);

		User user = null;
		if (username != null) {
			user = userService.getByUsername(username);
		}

		List<String> instanceIds = new ArrayList<>();
		if (sharecode != null) {
			instanceIds = sharecodeService.getInstanceIdsByCode(sharecode);
		}

		Specification<SurveyResult> specs = Specification.where(SurveyResultSpec.withSurvey(survey))
				.and(SurveyResultSpec.withDateBetween(from, to)).and(SurveyResultSpec.withUser(user))
				.and(SurveyResultSpec.withInstanceIds(instanceIds)).and(SurveyResultSpec.withArchived(archived));
		List<SurveyResult> results = surveyResultService.findAll(specs, new Sort(Direction.DESC, "modified"));

		JSONArray array = getJsonKeysAndValues(survey, results, groupDelimiter);
		long t1 = System.currentTimeMillis();
		System.out.println(
				"SurveyResultController.getKeysAndValues: took " + (t1 - t0) + "ms for Survey " + survey.getFormId());
		return array.toString();
	}

	private List<String> getKeys(Survey survey, String groupDelimiter) {
		String formFile = survey.getFormFile();
		String key = OursciUtils.getSurveyDefinitionPrefix(survey.getFormId()) + formFile;
		InputStream is = s3Wrapper.get(key);

		return JavaRosaUtils.getKeys(is, groupDelimiter);
	}

	private boolean shouldRevealAnonymizedFields(User user, SurveyResult result) {

		if (user == null) {
			return false;
		}

		// if(oursciUtils.isAdmin(user)) { return true; }

		if (result.getUser() != null) {
			if (result.getUser().getUsername().equals(user.getUsername())) {
				return true;
			}
		}

		if (result.getOrganization() != null) {
			for (OrganizationMember member : result.getOrganization().getMembers()) {
				if (member.getUser() == user && member.isAdmin()) {
					return true;
				}
			}
		}

		return false;
	}

	private void showName(OrderedJSONObject row) {
		try {
			String username = row.getString("metaUserID");
			row.put("metaUserID", username + " | " + userService.getByUsername(username).getDisplayName());
		} catch (Exception e) {
			// thats ok, no name shown here
		}
	}

	private JSONArray getJsonKeysAndValues(Survey survey, List<SurveyResult> results, String groupDelimiter)
			throws IOException {
		JSONArray array = new JSONArray();

		String formFile = survey.getFormFile();
		String formKey = OursciUtils.getSurveyDefinitionPrefix(survey.getFormId()) + formFile;
		FormDef formDef = JavaRosaUtils.getFormDef(s3Wrapper.get(formKey));

		User user = oursciUtils.getCurrentUserOrNull();

		if (concurrentDownloads) {
			// concurrent downloads
			List<String> keys = new ArrayList<>();
			List<Boolean> revealAnonList = new ArrayList<>();

			for (SurveyResult result : results) {
				String prefix = OursciUtils.getSurveyResultPrefix(result.getFormId(), result.getInstanceId());
				String key = prefix + result.getInstanceFile();
				keys.add(key);

				boolean revealAnon = shouldRevealAnonymizedFields(user, result);
				revealAnonList.add(revealAnon);

			}

			List<InputStream> streams = null;
			try {
				Long t0 = System.currentTimeMillis();
				streams = s3Wrapper.getMultiple(keys);
				Long t1 = System.currentTimeMillis();
				System.out.println("s3Wrapper.getMultiple: took " + (t1 - t0) + "ms to get " + keys.size()
						+ " files for Survey " + survey.getFormId());
			} catch (InterruptedException | ExecutionException e1) {
			}

			int idx = 0;
			for (InputStream stream : streams) {
				boolean revealAnon = false;
				try {
					revealAnon = revealAnonList.get(idx);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				OrderedJSONObject row = JavaRosaUtils.getJsonKeysAndValues(formDef, stream, revealAnon, groupDelimiter);

				if (revealAnon) {
					// TODO: find a better way for this
					showName(row);
				}
				try {
					array.put(row);
				} catch (Exception e) {

				}

				idx++;
			}
		} else {
			// sequential downloads
			for (SurveyResult result : results) {
				boolean revealAnon = shouldRevealAnonymizedFields(user, result);

				String prefix = OursciUtils.getSurveyResultPrefix(result.getFormId(), result.getInstanceId());
				String key = prefix + result.getInstanceFile();
				InputStream stream = s3Wrapper.get(key);

				OrderedJSONObject row = JavaRosaUtils.getJsonKeysAndValues(formDef, stream, revealAnon, groupDelimiter);
				if (revealAnon) {
					showName(row);
				}
				try {
					array.put(row);
				} catch (Exception e) {

				}
			}
		}

		return array;
	}

	@RequestMapping(value = "/by-database-id/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public SurveyResult getSurveyResultById(@PathVariable("id") Long id) {
		return surveyResultService.findOne(id);
	}

	@PostMapping(value = "/archive")
	public String archiveResults(@RequestBody String[] selection) {

		for (String instanceId : selection) {
			SurveyResult sr = surveyResultService.findByIdentifier(instanceId);
			if(sr == null) {
				continue;
			}

			boolean allowModification = false;

			User currentUser = oursciUtils.getCurrentUser();
			if (currentUser == null) {
				continue;
			}

			// Global admin
			if (oursciUtils.isAdmin(currentUser))
				allowModification = true;

			// Creator of the survey
			if (sr.getSurvey().getUser() == currentUser)
				allowModification = true;

			// Creator of the survey result
			if (sr.getUser() == currentUser)
				allowModification = true;

			if (!allowModification) {
				continue;
			}

			surveyResultService.archive(sr.getId()); // also archives survey results
		}

		return "ok";
	}

	@RequestMapping(value = "/{instanceId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public SurveyResult getSurveyResultById(@PathVariable("instanceId") String instanceId) {
		return surveyResultService.findByIdentifier(instanceId);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public SurveyResult createSurveyResult(@RequestParam("files") MultipartFile[] files,
			@RequestParam(required = false, defaultValue = "false") Boolean override,
			@RequestParam(required = false, defaultValue = "") String organizationId)
			throws XFormParseException, IOException, ParseException {

		FormInstance formInstance = null;
		String instanceFile = null;

		for (MultipartFile file : files) {
			if (file.getOriginalFilename().toLowerCase().endsWith(".xml")) {
				formInstance = JavaRosaUtils.getFormInstance(file.getInputStream());
				instanceFile = file.getOriginalFilename();
				break;
			}
		}

		if (formInstance == null || instanceFile == null) {
			throw new XmlFormNotFound("No form instance xml file in uploaded files");
		}

		String formId = JavaRosaUtils.getFormId(formInstance);
		Survey survey = surveyService.getByIdentifier(formId);

		String instanceId = JavaRosaUtils.getInstanceID(formInstance);
		Organization organization = null;
		if (!organizationId.isEmpty()) {
			try {
				organization = organizationRepository.findOneByOrganizationId(organizationId);
			} catch (Exception e) {
			}
		}

		String sharecodeEmail = JavaRosaUtils.getStringValue(formInstance, OursciUtils.OURSCI_SHARE_EMAIL);
		
		SurveyResult surveyResult = surveyResultRepository.findByInstanceId(instanceId);
		if (surveyResult != null) {
			if (override == false) {
				throw new SurveyResultExistsException("A survey result with instance id already exists: " + instanceId);
			}
			// In case this survey already exists, we do not want to override the user & organization
		} else {
			surveyResult = new SurveyResult();
			surveyResult.setUser(oursciUtils.getCurrentUserOrNull());
			surveyResult.setOrganization(organization);
		}

		
		String modifiedTimestamp, createdTimestamp;
		Date modified, created;

		modifiedTimestamp = JavaRosaUtils.getModifiedTimestamp(formInstance);
		createdTimestamp = JavaRosaUtils.getCreatedTimestamp(formInstance);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

		try {
			modified = sdf.parse(modifiedTimestamp);
		} catch (Exception e) {
			modified = new Date();
		}

		try {
			created = sdf.parse(createdTimestamp);
		} catch (Exception e) {
			created = new Date();
		}

		// persist on S3
		String prefix = OursciUtils.getSurveyResultPrefix(formId, instanceId);
		s3Wrapper.upload(files, prefix);

		// persist in database
		surveyResult.setModified(modified);
		surveyResult.setModifiedTimestamp(modifiedTimestamp);
		surveyResult.setCreated(created);
		surveyResult.setCreatedTimestamp(createdTimestamp);
		surveyResult.setFormId(formId);
		surveyResult.setInstanceId(instanceId);
		surveyResult.setInstanceFile(instanceFile);
		surveyResult.setSurvey(survey);
		
		// update latest contribution field on survey
		survey.setLatestContribution(modified);
		surveyService.save(survey);

		// persist
		surveyResult = surveyResultRepository.save(surveyResult);

		// create share code?
		if (sharecodeEmail != null && !sharecodeEmail.trim().isEmpty()) {
			Dashboard dashboard = null;
			String sharecodeDashboard = JavaRosaUtils.getStringValue(formInstance, OursciUtils.OURSCI_SHARE_DASHBOARD);
			if (sharecodeDashboard != null && !sharecodeDashboard.trim().isEmpty()) {
				try {
					dashboard = dashboardService.findByIdentifier(sharecodeDashboard);
				} catch (DashboardNotFound dnf) {
					dashboard = null;
				}
			}

			if (sharecodeService.getByEmail(sharecodeEmail).size() == 0) {
				// automatically send an e-mail if this is the first share
				sharecodeService.sendLink(sharecodeEmail);
			}

			Sharecode existingSharecode = sharecodeService.find(surveyResult, dashboard, sharecodeEmail);
			if (existingSharecode == null || existingSharecode.isArchived()) {
				sharecodeService.add(surveyResult, dashboard, sharecodeEmail, Sharecode.CLAIM); // add will also unarchive
			}

		}

		// send statistics to websocket
		if (!override) {
			Statistics stats = new Statistics();
			stats.setSurveys(surveyService.count());
			stats.setSurveyResults(surveyResultService.count() + 1); // we are adding one here
			webSocket.convertAndSend("/topic/stats", stats);
		}

		return surveyResult;
	}

	@RequestMapping("/users/by-form-id/{formId}")
	public List<User> getUserList(@PathVariable String formId) {
		return surveyResultRepository.getUserByFormId(formId);
	}

	@RequestMapping("/contribution/user/{username}")
	Page<Contribution> getContributionsByUser(@PathVariable String username, Pageable pageable) {
		User user = userService.getByUsername(username);
		Page<Contribution> ret = surveyResultRepository.findContributionsByUser(user, pageable);
		return ret;
	}

}