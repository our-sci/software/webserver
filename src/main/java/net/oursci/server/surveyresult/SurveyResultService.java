package net.oursci.server.surveyresult;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import net.oursci.server.exception.BaseException;
import net.oursci.server.survey.Survey;
import net.oursci.server.user.User;

public interface SurveyResultService {
	long count();
	
	SurveyResult findOne(Long id) throws BaseException;
	SurveyResult save(SurveyResult surveyResult);
	SurveyResult findByIdentifier(String identifier);
	List<SurveyResult> findByInstanceId(List<String> instanceIds);
	
	void archive(Long id);
	
	List<SurveyResult> find(Survey survey);
	List<SurveyResult> findByDate(Survey survey, Date from, Date to);
	
	List<SurveyResult> find(Survey survey, Pageable pageable);
	List<SurveyResult> findByDate(Survey survey, Date from, Date to, Pageable pageable);

	List<SurveyResult> findByUser(Survey survey, User user);
	List<SurveyResult> findByUser(Survey survey, User user, Pageable pageable);
	
	SurveyResult getLastContribution(User user);
	
	List<SurveyResult> findAll(Specification<SurveyResult> spec);
	List<SurveyResult> findAll(Specification<SurveyResult> spec, Sort sort);
	
}
