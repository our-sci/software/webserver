package net.oursci.server.surveyresult;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.BaseException;
import net.oursci.server.exception.SurveyResultNotFound;
import net.oursci.server.sharecode.Sharecode;
import net.oursci.server.sharecode.SharecodeService;
import net.oursci.server.survey.Survey;
import net.oursci.server.user.User;

@Service
public class SurveyResultServiceImpl implements SurveyResultService {

	@Autowired
	SurveyResultRepository repo;
	
	@Autowired
	SharecodeService sharecodeService;
	
	@Override
	public long count() {
		return repo.count();
	}
	
	@Override
	public SurveyResult findOne(Long id) throws BaseException {
		return repo.findById(id).orElseThrow(() -> new SurveyResultNotFound());
	}

	@Override
	public SurveyResult save(SurveyResult surveyResult) {
		return repo.save(surveyResult);
	}

	@Override
	public SurveyResult findByIdentifier(String identifier) {
		SurveyResult surveyResult = repo.findByInstanceId(identifier);
		if(surveyResult == null) {
			throw new SurveyResultNotFound();
		}
		return surveyResult;
	}

	@Override
	public List<SurveyResult> find(Survey survey) {
		return repo.findBySurveyAndArchivedFalseOrderByModifiedDesc(survey);
	}

	@Override
	public List<SurveyResult> find(Survey survey, Pageable pageable) {
		return repo.findBySurveyAndArchivedFalseOrderByModifiedDesc(survey, pageable);

	}

	@Override
	public List<SurveyResult> findByDate(Survey survey, Date from, Date to) {
		if(from == null || to == null) {
			return find(survey);
		} 
		to = DateUtils.addDays(to, 1);
		
		return repo.findBySurveyAndModifiedBetweenAndArchivedFalseOrderByModifiedDesc(survey, from, to);
	}

	@Override
	public List<SurveyResult> findByDate(Survey survey, Date from, Date to, Pageable pageable) {
		if(from == null || to == null) {
			return find(survey, pageable);
		} 
		to = DateUtils.addDays(to, 1);
		
		return repo.findBySurveyAndModifiedBetweenAndArchivedFalseOrderByModifiedDesc(survey, from, to, pageable);
	}

	@Override
	public SurveyResult getLastContribution(User user) {
		return repo.findTopByUserOrderByModifiedDesc(user);
	}

	@Override
	public List<SurveyResult> findByUser(Survey survey, User user) {
		return repo.findBySurveyAndUserAndArchivedFalseOrderByModifiedDesc(survey, user);
	}
	
	@Override
	public List<SurveyResult> findByUser(Survey survey, User user, Pageable pageable) {
		return repo.findBySurveyAndUserAndArchivedFalseOrderByModifiedDesc(survey, user, pageable);
	}
	
	@Override
	public List<SurveyResult> findAll(Specification<SurveyResult> spec) {
		return repo.findAll(spec);
	}

	@Override
	public List<SurveyResult> findAll(Specification<SurveyResult> spec, Sort sort) {
		return repo.findAll(spec, sort);
	}

	@Override
	public List<SurveyResult> findByInstanceId(List<String> instanceIds) {
		return repo.findByInstanceIdIn(instanceIds);
	}

	@Override
	public void archive(Long id) {
		SurveyResult surveyResult = findOne(id);
		List<Sharecode> associatedSharecodes = sharecodeService.getBySurveyResult(surveyResult);
		for(Sharecode sharecode: associatedSharecodes) {
			sharecodeService.archive(sharecode.getId());
		}
		surveyResult.setArchived(true);
		repo.save(surveyResult);
	}





}
