package net.oursci.server.surveyresult;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.organization.Organization;
import net.oursci.server.survey.Survey;
import net.oursci.server.user.User;
import net.oursci.server.views.Views;

@Entity
@Table(name="surveyresult")
@Data
public class SurveyResult {
	
	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;
	
	@Column(name = "modified")
	@JsonView(Views.Summary.class)
	private Date modified;
	
	@Column(name = "modifiedtimestamp")
	@JsonView(Views.Summary.class)
	private String modifiedTimestamp;
	
	@Column(name = "created")
	@JsonView(Views.Summary.class)
	private Date created;
	
	@Column(name = "createdtimestamp")
	@JsonView(Views.Summary.class)
	private String createdTimestamp;
	
	@Column(name = "formid")
	@JsonView(Views.Summary.class)
	private String formId;
	
	@Column(name = "formversion")
	@JsonView(Views.Summary.class)
	private String formVersion;
	
	@Column(name = "instanceid", unique = true)
	@JsonView(Views.Summary.class)
	private String instanceId;
	
	@Column(name = "instancefile")
	@JsonView(Views.Summary.class)
	private String instanceFile;
	
	@NotNull
	@OneToOne
	@JsonView(Views.Summary.class)
	private Survey survey;
	
	@OneToOne
	private User user;
	
	@OneToOne
	@JsonView(Views.Summary.class)
	private Organization organization;
	
	@JsonView(Views.Summary.class)
	private boolean publicDomain = true;
	
	private boolean archived = false;
		
}
