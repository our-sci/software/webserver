package net.oursci.server.mail;

public interface MailService {
	public boolean sendMail(String to, String subject, String body);
}
