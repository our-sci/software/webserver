package net.oursci.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.firebase.FirebaseService;
import net.oursci.server.firebase.FirebaseTokenHolder;
import net.oursci.server.user.RegisterUserInit;
import net.oursci.server.user.UserService;
import net.oursci.server.util.StringUtil;

@RestController
public class SignupController {
	
	@Autowired
	private FirebaseService firebaseService;
	
	@Autowired
	private UserService userService;
	

	@RequestMapping(value = "/api/signup", method = RequestMethod.POST)
	public void signUp(@RequestHeader String firebaseToken) {
		if (StringUtil.isBlank(firebaseToken)) {
			throw new IllegalArgumentException("FirebaseTokenBlank");
		}
		FirebaseTokenHolder tokenHolder = firebaseService.parseToken(firebaseToken);
		userService.registerUser(new RegisterUserInit(tokenHolder.getUid(), tokenHolder.getEmail(), tokenHolder.getName(), tokenHolder.getPicture()));
	}
	
}