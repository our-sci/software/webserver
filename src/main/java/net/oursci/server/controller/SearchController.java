package net.oursci.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.dashboard.DashboardService;
import net.oursci.server.mappings.SearchResult;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.script.Script;
import net.oursci.server.script.ScriptService;
import net.oursci.server.survey.Survey;
import net.oursci.server.survey.SurveyService;

@RestController
@RequestMapping(value = "/api/find")
public class SearchController {

	@Autowired
	SurveyService surveyService;
	
	@Autowired
	ScriptService scriptService;
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	OrganizationService organizationService;
	
	@RequestMapping
	public SearchResult search(@RequestParam String search, Pageable pageable) {
		
		Page<Survey> surveys = surveyService.find(search, pageable);
		Page<Script> scripts = scriptService.find(search, pageable);
		Page<Dashboard> dashboards = dashboardService.find(search, pageable);
		Page<Organization> organizations = organizationService.find(search, pageable);
		
		SearchResult sr = new SearchResult();
		
		sr.setSurveys(surveys);
		sr.setScripts(scripts);
		sr.setDashboards(dashboards);
		sr.setOrganizations(organizations);
		
		return sr;
	}
	
}
